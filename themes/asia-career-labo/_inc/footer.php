<a
  href="#"
  class="arrow-button arrow-button--top arrow-button--page-scroll"
  data-page-scroll
>
  <i class="arrow-button__icon"></i>
</a>
<footer 
class="footer
<?php if ( is_page('about') ) { echo 'footer--about'; } ?>" 
data-footer>
  <div class="footer__top">
    <ul class="footer__banners">
      <li class="footer__banner">
        <a href="https://line.me/R/ti/p/%40936fgdso" target="_blank" class="footer__anchor">
          <img
            src="<?php echo THEME_IMAGE ?>banner/footer_line.png"
            alt=""
            class="footer__banner-image"
            loading="lazy"
          />
        </a>
      </li>
      <li class="footer__banner">
        <a href="https://www.youtube.com/channel/UC4FiUQwkabHKc7islFWF_7A" target="_blank" class="footer__anchor">
          <img
            src="<?php echo THEME_IMAGE ?>banner/footer_youtube_001.png"
            alt=""
            class="footer__banner-image"
            loading="lazy"
          />
        </a>
      </li>
      <li class="footer__banner">
        <a href="https://www.youtube.com/channel/UCKHaRzXXjKZcQDaPfgBzz6g" target="_blank" class="footer__anchor">
          <img
            src="<?php echo THEME_IMAGE ?>banner/footer_youtube_002.png"
            alt=""
            class="footer__banner-image"
            loading="lazy"
          />
        </a>
      </li>
    </ul>
  </div>
  <div class="footer__bottom">
    <a href="<?php echo HOME_URI ?>" class="footer__anchor footer__anchor--logo">
      <svg class="footer__logo">
        <title>ASIA CAREER LABO</title>
        <use
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#logotype"
        ></use>
      </svg>
    </a>
    <ul class="footer__list">
      <li class="footer__item">
        <a href="<?php echo HOME_URI ?>/company/" class="footer__anchor"> 運用会社 </a>
      </li>
      <li class="footer__item">
        <a href="<?php echo HOME_URI ?>/term/" class="footer__anchor"> 利用規約 </a>
      </li>
      <li class="footer__item">
        <a href="<?php echo HOME_URI ?>/privacy/" class="footer__anchor"> プライバシーポリシー </a>
      </li>
      <li class="footer__item">
        <a href="<?php echo HOME_URI ?>/contact/" class="footer__anchor"> お問い合わせ </a>
      </li>
    </ul>
    <ul class="footer__sns">
      <li class="footer__sns-item">
        <a href="https://twitter.com/asia_labo" target="_blank" rel="noopener noreferrer" class="footer__anchor footer__anchor--icon">
          <svg class="footer__icon">
            <title>Twitter</title>
            <use
              xmlns:xlink="http://www.w3.org/1999/xlink"
              xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#twitter"
            ></use>
          </svg>
        </a>
      </li>
      <li class="footer__sns-item">
        <a href="https://www.instagram.com/asia.labo/" target="_blank" rel="noopener noreferrer" class="footer__anchor footer__anchor--icon">
          <svg class="footer__icon">
            <title>Instagram</title>
            <use
              xmlns:xlink="http://www.w3.org/1999/xlink"
              xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#instagram"
            ></use>
          </svg>
        </a>
      </li>
      <li class="footer__sns-item">
        <a href="https://line.me/R/ti/p/%40936fgdso" target="_blank" rel="noopener noreferrer" class="footer__anchor footer__anchor--icon">
          <svg class="footer__icon">
            <title>LINE</title>
            <use
              xmlns:xlink="http://www.w3.org/1999/xlink"
              xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#line"
            ></use>
          </svg>
        </a>
      </li>
    </ul>
    <small class="footer__copyright">©︎ <?php echo date('Y'); ?> asia career labo</small>
  </div>
</footer>
