<aside class="aside-nav" data-aside-nav>
    <h2 class="heading-secondary heading-secondary--aside-nav">
    <span class="heading-secondary__main">CATEGORY</span>
    <span class="heading-secondary__sub">カテゴリ別新着記事</span>
    </h2>

    <!-- 表示カテゴリ(slugで指定) -->
    <?php 
        $cat_parents = array(
            'kyujin',   //求人情報
            'shushoku', //就職／転職
            'life',     //ライフスタイル
            'migration' //海外移住
        );
    ?>
    <?php foreach($cat_parents as $cat_parent): ?>
        <?php 
            $cat_slug = get_category_by_slug($cat_parent);
            switch ($cat_parent){
                case 'kyujin':
                    $label_color = 'label--green';
                    break;
                case 'shushoku':
                    $label_color = 'label--blue';
                    break;
                case 'life':
                    $label_color = 'label--pink';
                    break;
                case 'migration':
                    $label_color = 'label--violet';
                    break;
                default:
            }
        ?>
        <h3 class="card-list-header">
            <span class="label <?php echo $label_color; ?>">
            <?php echo $cat_slug -> name; ?></span>
            <a href="<?php echo HOME_URI . '/category/' . $cat_parent . '/' ?>" class="button-primary button-primary--in-heading">
                すべて見る
            </a>
        </h3>
        <ul class="card-list card-list--aside-nav">
        <?php
            $cat_posts = get_posts(
                array(
                    'post_type' => 'post', // 投稿タイプ
                    'category' => $cat_slug -> term_id, // カテゴリIDを番号で指定する場合
                    'category_name' => $cat_parent, // カテゴリをスラッグで指定する場合
                    'has_password' => false, // パスワード保護投稿を除外
                    'posts_per_page' => 3, // 表示件数
                    'orderby' => 'date', // 表示順の基準
                    'order' => 'DESC' // 昇順・降順
                )
            );
            global $post;
            if($cat_posts): foreach($cat_posts as $post): setup_postdata($post);
         ?>
        <li class="card-horizontal">
            <a href="<?php the_permalink() ?>" class="card-horizontal__anchor">
            <figure class="card-horizontal__image-wrap">
            <?php if (has_post_thumbnail()): ?>
                <?php the_post_thumbnail( 'full', 
                    array( 
                        'class' => 'card-horizontal__image',
                        'alt' => '',
                        'loading' => 'lazy'
                    ) ); 
                ?>
            <?php elseif (!has_post_thumbnail() && $cat_parent === 'kyujin' ): ?>
                <img src="<?php echo THEME_IMAGE ?>job_no-image.png" alt="" class="card-horizontal__image" loading="lazy">
            <?php else: ?>
                <img src="<?php echo THEME_IMAGE ?>no-image.png" alt="" class="card-horizontal__image" loading="lazy">
            <?php endif; ?>
            </figure>
            <dl class="card-horizontal__summary">
                <dt class="card-horizontal__term">
                    <?php the_title(); ?>
                </dt>
                <dd class="card-horizontal__description">
                <figure class="card-horizontal__avater-wrap">
                    <?php echo get_avatar( get_the_author_id(), 96, '', '', $args = array( 'class' => 'card-horizontal__avater' ) ); ?>
                </figure>
                <div class="card-horizontal__author-wrap">
                    <span class="card-horizontal__author"
                    ><?php the_author(); ?></span
                    >
                    <time class="card-horizontal__date"><?php the_time('Y.m.d') ?></time>
                </div>
                </dd>
            </dl>
            </a>
        </li>
        <?php endforeach; endif; wp_reset_postdata(); ?>
    </ul>
    <?php endforeach; ?>

    <!-- 人気記事 WordPress Popular Posts -->
    <div class="aside-nav__sticky">
        <h2 class="heading-secondary heading-secondary--aside-nav">
            <span class="heading-secondary__main">POPULAR</span>
            <span class="heading-secondary__sub">人気の記事</span>
        </h2>
        <?php
        $wpp = array (
            'range' => 'all',
            'limit' => 10,
            'post_type' => 'post',
            'stats_date' => true,
            'stats_date_format' => 'Y.m.d',
            'stats_author' => true,
            'thumbnail_width' => 400, 
            'thumbnail_height' => 267, 
            'wpp_start' => '<ol class="card-list card-list--aside-nav">',
            'wpp_end' => '</ol>',
            'post_html' => '<li class="card-horizontal card-horizontal--popular">
                                <a href="{url}" class="card-horizontal__anchor">
                                    <i class="card-horizontal--popular__badge"></i>
                                    <figure class="card-horizontal__image-wrap">
                                    <img
                                        src="{thumb_url}"
                                        alt=""
                                        class="card-horizontal__image"
                                        loading="lazy"
                                    />
                                    </figure>
                                    <dl class="card-horizontal__summary">
                                    <dt class="card-horizontal__term">
                                        {text_title}
                                    </dt>
                                    <dd class="card-horizontal__description">
                                        <figure class="card-horizontal__avater-wrap">
                                            {avatar}
                                        </figure>
                                        <div class="card-horizontal__author-wrap">
                                            <span class="card-horizontal__author">{text_author}</span>
                                            <time class="card-horizontal__date">{date}</time>
                                        </div>
                                    </dd>
                                    </dl>
                                </a>
                            </li>',
        ); ?>
        <?php wpp_get_mostpopular($wpp); ?>
    </div>
</aside>