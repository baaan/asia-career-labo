<input type="checkbox" id="menu" class="menu-toggle" autocomplete="off" />
<label for="menu" class="menu-button">
    <div class="menu-button__icon">
    <i></i>
    <i></i>
    <i></i>
    </div>
    <small class="menu-button__text"></small>
</label>
<div class="overlay"></div>
<nav class="global-nav" data-global-nav>
    <div class="global-nav__inner">
        <ul class="global-nav__list">
            <li class="global-nav__item">
            <a href="<?php echo HOME_URI ?>" class="global-nav__anchor"> TOP </a>
            </li>
            <li class="global-nav__item">
            <a href="<?php echo HOME_URI ?>/about/" class="global-nav__anchor"> ABOUT </a>
            </li>
            <li class="global-nav__item">
            <a href="<?php echo HOME_URI ?>/author/" class="global-nav__anchor"> WRITER </a>
            </li>
            <li class="global-nav__item">
            <a href="<?php echo HOME_URI ?>/event/" class="global-nav__anchor"> EVENT </a>
            </li>
            <li class="global-nav__item">
            <a href="https://www.youtube.com/channel/UC4FiUQwkabHKc7islFWF_7A" target="_blank" rel="noopener noreferrer" class="global-nav__anchor"> YOUTUBE </a>
            </li>
            <li class="global-nav__item global-nav__item--category">
                <a
                    href="<?php echo HOME_URI ?>/category/"
                    class="global-nav__anchor global-nav__anchor--categories"
                    >CATEGORY</a
                >
                <i class="global-nav__arrow"></i>
                <input
                    type="checkbox"
                    id="accordion"
                    class="accordion-toggle"
                    autocomplete="off"
                />
                <label for="accordion" class="accordion-button"></label>
                <ul class="global-nav__categories">
                    <!-- 大カテゴリ -->
                    <?php 
                    $cat_parents = array(
                        'kyujin',
                        'shushoku',
                        'life',
                        'migration',
                        'business'
                    );
                    ?>
                    <?php foreach($cat_parents as $cat_parent): ?>
                    <?php $cat_slug = get_category_by_slug($cat_parent); ?>
                    <li class="global-nav__category">
                        <a
                            href="<?php echo HOME_URI . '/category/' . $cat_parent . '/' ?>"
                            class="global-nav__anchor global-nav__anchor--category"
                        >
                            <?php echo $cat_slug -> name; ?>
                        </a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </li>
            <li class="global-nav__item">
            <a href="<?php echo HOME_URI ?>/contact/" class="global-nav__anchor"> CONTACT </a>
            </li>
        </ul>
        <ul class="global-nav__sns">
            <li class="global-nav__sns-item">
            <a href="https://twitter.com/asia_labo" target="_blank" rel="noopener noreferrer" class="global-nav__anchor global-nav__anchor--sns">
                <svg class="global-nav__icon">
                <title>Twitter</title>
                <use
                    xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#twitter"
                ></use>
                </svg>
            </a>
            </li>
            <li class="global-nav__sns-item">
            <a href="https://www.youtube.com/channel/UC4FiUQwkabHKc7islFWF_7A" target="_blank" rel="noopener noreferrer" class="global-nav__anchor global-nav__anchor--sns">
                <svg class="global-nav__icon">
                <title>Instagram</title>
                <use
                    xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#instagram"
                ></use>
                </svg>
            </a>
            </li>
            <li class="global-nav__sns-item">
            <a href="https://line.me/R/ti/p/%40936fgdso" target="_blank" rel="noopener noreferrer" class="global-nav__anchor global-nav__anchor--sns">
                <svg class="global-nav__icon">
                <title>LINE</title>
                <use
                    xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#line"
                ></use>
                </svg>
            </a>
            </li>
            <li class="global-nav__sns-item">
            <a href="https://www.youtube.com/channel/UC4FiUQwkabHKc7islFWF_7A" target="_blank" rel="noopener noreferrer" class="global-nav__anchor global-nav__anchor--sns">
                <svg class="global-nav__icon">
                <title>youtube</title>
                <use
                    xmlns:xlink="http://www.w3.org/1999/xlink"
                    xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#youtube"
                ></use>
                </svg>
            </a>
            </li>
        </ul>
    </div>
</nav>