<meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
<meta content="#FFF734" name="theme-color" />
<meta content="#FFF734" name="msapplication-TileColor" />
<meta content="#FFF734" name="theme-color" />
<?php if ( is_singular('form') ): ?>
<meta name="robots" content="noindex, nofollow">
<?php endif; ?>
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo THEME_IMAGE ?>/apple-touch-icon.png" />
<link rel="icon" href="<?php echo THEME_IMAGE ?>/favicon.png" />
<link rel="preconnect" href="https://fonts.googleapis.com" />
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
<link href="https://fonts.googleapis.com/css2?family=Heebo:wght@500;700&display=swap" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;700&display=swap" rel="stylesheet" />
<?php wp_head(); ?>