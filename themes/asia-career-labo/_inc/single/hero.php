<?php if ( have_posts() ) : ?>
    <?php while( have_posts() ) : the_post(); ?>
    <div class="hero">
        <?php
            //  カテゴリ名・リンク
            $cat_id = get_the_category()[0] -> cat_ID;
            $cat_name = get_the_category()[0] -> cat_name;
            $cat_slug = get_the_category()[0] -> slug;
            $cat_href = esc_url(get_category_link($cat_id));

            // 大カテゴリラベル
            $path = $_SERVER['REQUEST_URI'];
            if (strpos($path, 'kyujin')) {
                $label_color = 'label--green';
            } elseif (strpos($path, 'shushoku')) {
                $label_color = 'label--blue';
            } elseif (strpos($path, 'migration')) {
                $label_color = 'label--violet';
            } elseif (strpos($path, 'life')) {
                $label_color = 'label--pink';
            }
        ?>

        <div class="hero__image-wrap">
            <?php if (has_post_thumbnail()): ?>
                <?php the_post_thumbnail( 'full', 
                array( 
                    'class' => 'hero__image',
                    'alt' => '',
                    'loading' => 'eager'
                ) ); 
                ?>
            <?php elseif (!has_post_thumbnail() && strpos($path, 'kyujin') ): ?>
                <img src="<?php echo THEME_IMAGE ?>job_no-image.png" alt="" class="hero__image" loading="eager">
            <?php else: ?>
                <img src="<?php echo THEME_IMAGE ?>no-image.png" alt="" class="hero__image" loading="eager">
            <?php endif; ?>
            <?php if( 'event' === get_post_type() ): ?>
            <a 
                href="<?php echo HOME_URI ?>/event/" 
                class="label">
                イベント情報
            </a>
            <?php else: ?>
            <a 
                href="<?php echo $cat_href; ?>" 
                class="label <?php echo $label_color; ?>">
                <?php echo $cat_name ?>
            </a>
            <?php endif; ?>
        </div>

        <div class="hero__inner">

            <?php if( 'event' === get_post_type() ): ?>
            <!-- 開催日/時間/曜日(イベント情報) START -->
            <?php 
                $date = date_create(get_field('event_yymmdd'));
                $year  = date_format($date,'Y');
                $month = date_format($date,'m');
                $day   = date_format($date,'d');
                $datetime = new DateTime();
                $datetime -> setDate($year, $month, $day);
                $week = array("日", "月", "火", "水", "木", "金", "土");
                $w = (int)$datetime -> format('w');
            ?>
            <div class="hero__schedule">
                <span class="hero__year">
                    <?php echo date_format($date,'Y'); ?>
                </span>
                <div class="hero__schedule-inner">
                  <span class="hero__month"><?php echo date_format($date,'m'); ?></span>
                  <span class="hero__date"><?php echo date_format($date,'d'); ?></span>
                  <span class="hero__dateofweek"><?php echo $week[$w]; ?></span>
                  <span class="hero__placeholder-time">
                    <time class="hero__start-time"><?php the_field('event_start_time'); ?></time
                    ><time class="hero__finish-time"><?php the_field('event_finish_time'); ?></time>
                  </span>
                </div>
            </div>
            <!-- 開催日/時間/曜日(イベント情報) END -->
            <?php endif; ?>

            <h1 class="hero__heading">
                <?php the_title(); ?>
            </h1>

            <?php if( 'event' === get_post_type() ): ?>
            <!-- 開催地(イベント情報) STRAT -->
            <p class="hero__place"><?php the_field('event_place_text') ?></p>
            <!-- 開催地(イベント情報) END -->
            <?php endif; ?>

            <!-- Tag -->
            <?php 
                $tags = wp_get_post_tags($post->ID);
            ?>
            <?php if ($tags): ?>
            <div class="hero__tag">
                <?php foreach($tags as $tag): ?>
                    <a href="<?php echo $HOME_URI . '/tag/' . $tag -> slug ?>" class="tag">
                        <?php echo $tag -> name ?>
                    </a>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>

            <?php if( 'event' !== get_post_type() ): ?>
            <div class="hero__author">
                <a href="<?php echo get_author_posts_url( get_the_author_id() ); ?>" class="hero__anchor">
                    <?php echo get_avatar( get_the_author_id(), 96, '', '', $args = array( 'class' => 'hero__avater' ) ); ?>
                </a>
                <div class="hero__caption">
                    <a href="<?php echo get_author_posts_url( get_the_author_id() ); ?>" class="hero__name"><?php the_author(); ?></a>
                    <time class="hero__update"><?php the_time('Y.m.d') ?></time>
                </div>
            </div>
            <?php endif; ?>

        </div>
    </div>
    <?php endwhile;?>
<?php endif; ?>