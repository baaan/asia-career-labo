<header 
class="header 
<?php 
    if ( is_page('about') ) { 
        echo 'header--transparent'; 
    } else if ( !is_home() || !is_front_page() ) { 
        echo 'header--simple';
    } 
?>" 
data-header>
  <a href="<?php echo HOME_URI ?>" class="header__anchor">
    <p class="header__catchcopy">
      アジラボ
      <span class="header__catchcopy-line">|</span>
      飛び出せ日本！海外転職応援メディア
    </p>
    <picture class="header__logo">
      <source
        media="(max-width: 1105px)"
        srcset="<?php echo THEME_IMAGE ?>logotype.svg"
        class="header__image"
      />
      <img
        src="<?php echo THEME_IMAGE ?>logotype_vertical.svg"
        alt="ASIA CAREER LABO"
        class="header__image"
      />
    </picture>
  </a>
  <ul class="header__sns">
    <li class="header__sns-item">
      <a href="https://twitter.com/asia_labo" target="_blank" rel="noopener noreferrer" class="header__anchor header__anchor--icon">
        <svg class="header__icon">
          <title>Twitter</title>
          <use
            xmlns:xlink="http://www.w3.org/1999/xlink"
            xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#twitter"
          ></use>
        </svg>
      </a>
    </li>
    <li class="header__sns-item">
      <a href="https://www.instagram.com/asia.labo/" target="_blank" rel="noopener noreferrer" class="header__anchor header__anchor--icon">
        <svg class="header__icon">
          <title>Instagram</title>
          <use
            xmlns:xlink="http://www.w3.org/1999/xlink"
            xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#instagram"
          ></use>
        </svg>
      </a>
    </li>
    <li class="header__sns-item">
      <a href="https://line.me/R/ti/p/%40936fgdso" target="_blank" rel="noopener noreferrer" class="header__anchor header__anchor--icon">
        <svg class="header__icon">
          <title>LINE</title>
          <use
            xmlns:xlink="http://www.w3.org/1999/xlink"
            xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#line"
          ></use>
        </svg>
      </a>
    </li>
    <li class="header__sns-item">
      <a href="https://www.youtube.com/channel/UC4FiUQwkabHKc7islFWF_7A" target="_blank" rel="noopener noreferrer" class="header__anchor header__anchor--icon">
        <svg class="header__icon">
          <title>youtube</title>
          <use
            xmlns:xlink="http://www.w3.org/1999/xlink"
            xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#youtube"
          ></use>
        </svg>
      </a>
    </li>
  </ul>
</header>
