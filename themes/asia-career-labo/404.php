<!DOCTYPE html>
<html lang="ja">
<head>
    <!-- head -->
    <?php get_template_part('_inc/head'); ?>
</head>
<body class="not-found">
    <!-- header -->
    <?php get_template_part('_inc/header'); ?>
    <!-- global_nav -->
    <?php get_template_part('_inc/global_nav'); ?>

    <!-- content -->
    <div class="wrapper">
      <div class="wrapper__inner" data-wrapper>
        <main class="main" data-main>
          <section class="section section--404">
            <div class="not-found__content">
              <div class="not-found__symbol">404</div>
              <p class="not-found__text">ページが見つかりません</p>

              <a href="<?php echo HOME_URI ?>" class="button-primary">トップへ戻る</a>
            </div>
          </section>
        </main>
      </div>
    </div>
    <!-- /content -->

    <!-- footer -->
    <?php get_template_part('_inc/footer'); ?>
    <?php wp_footer(); ?>
</body>
</html>