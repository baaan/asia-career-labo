<!DOCTYPE html>
<html lang="ja">
<head>
    <!-- head -->
    <?php get_template_part('_inc/head'); ?>
</head>
<body>
    <!-- header -->
    <?php get_template_part('_inc/header'); ?>
    <!-- global_nav -->
    <?php get_template_part('_inc/global_nav'); ?>

    <!-- content -->
    <div class="wrapper wrapper--category-posts">
        <div class="wrapper__inner wrapper__inner--category" data-wrapper>
            <main class="main" data-main>
                <section class="section">
                    <h1 class="heading-primary heading-primary--category-page">
                        <span class="heading-primary__sub">CATEGORY</span>
                        <span class="heading-primary__main"><?php single_cat_title() ?></span>
                    </h1>
                    <?php 
                        // 現在のグローバルクエリを取得
                        global $wp_query;
                        // 直前のクエリーオブジェクトを取得
                        $cat_obj = $wp_query -> get_queried_object();
                        // 現在表示中のカテゴリー名
                        $cat_name = $cat_obj -> name;
                        // 現在表示中のカテゴリーID
                        $cat_id = $cat_obj -> term_id;
                        // 現在表示中のカテゴリーのスラッグ
                        $cat_slug = $cat_obj -> slug;
                        // 現在表示中の親カテゴリーID
                        $cat_parent_id = $cat_obj -> parent;
                        // 現在表示中の親カテゴリーのスラッグ
                        $cat_parent_slug = get_category($cat_parent_id) -> slug;
                    ?>
                    <?php //var_dump($cat_parent_slug); // 訪問中カテゴリの最上位親カテゴリ確認用 ?>
                    <!-- tab -->
                    <div class="tab" data-tab>
                        <?php
                            // 除外
                            $kyujin_id = get_category_by_slug('kyujin') -> term_id;
                            $shushoku_id = get_category_by_slug('shushoku') -> term_id;
                            $life_id = get_category_by_slug('life') -> term_id;
                            $migration_id = get_category_by_slug('migration') -> term_id;

                            // $thailand_id = get_category_by_slug('thailand') -> term_id;
                            // $work_id = get_category_by_slug('work') -> term_id;
                            // $business_id = get_category_by_slug('business') -> term_id;
                            // $new_id = get_category_by_slug('new') -> term_id;
                            // $event_id = get_category_by_slug('event') -> term_id;

                            // 大カテゴリラベル
                            $path = $_SERVER['REQUEST_URI'];
                            //echo $path;
                            if (strpos($path, 'kyujin')) {
                                $label_color = 'label--green';
                                $cat_root_id = $kyujin_id;
                                $cat_root_slug = 'kyujin';
                            } elseif (strpos($path, 'shushoku')) {
                                $label_color = 'label--blue';
                                $cat_root_id = $shushoku_id;
                                $cat_root_slug = 'shushoku';
                            } elseif (strpos($path, 'migration')) {
                                $label_color = 'label--violet';
                                $cat_root_id = $migration_id;
                                $cat_root_slug = 'migration';
                            } elseif (strpos($path, 'life')) {
                                $label_color = 'label--pink';
                                $cat_root_id = $life_id;
                                $cat_root_slug = 'life';
                            }

                            $cats_related = get_categories(array(
                                'orderby' => 'count',//ソート規則
                                'order' => 'DESC',
                                'child_of' => $cat_root_id,
                                // 'exclude' => $kyujin_id . ',' 
                                // . $shushoku_id . ',' 
                                // . $life_id . ',' 
                                // . $migration_id . ',' 
                                // // . $thailand_id . ',' 
                                // // . $work_id . ',' 
                                // // . $new_id . ',' 
                                // // . $business_id . ',' 
                                // // . $event_id,

                            ));

                        ?>
                        <ul class="tab__list" data-tab-list>
                            <li class="tab__item <?php $cat_parent_slug === NULL || $cat_parent_slug === 'thailand' ? print 'is-current' : ''; ?>" data-tab-item>
                                <a 
                                    href="<?php echo HOME_URI . '/category/' . $cat_root_slug . '/' ?>" 
                                    class="tab__anchor label <?php $cat_parent_slug === NULL || $cat_parent_slug === 'thailand' ? print $label_color : ''; ?>"
                                >
                                    すべての記事
                                </a>
                            </li>
                            <?php foreach($cats_related as $cat): ?>
                                <?php if( get_term_children( $cat_root_id, 'category' ) ): ?>
                                <li class="tab__item <?php $cat_slug === $cat -> slug ? print 'is-current' : ''; ?>">
                                    <a 
                                        href="<?php echo esc_url(get_category_link($cat -> term_id)); ?>" 
                                        class="tab__anchor label <?php $cat_slug === $cat -> slug ? print $label_color : ''; ?>
                                    ">
                                        <?php echo $cat -> name; ?>
                                    </a>
                                </li>
                                <?php endif ?>
                            <?php endforeach ?>
                        </ul>
                    </div>
                    <!-- /tab -->
                    <div class="section__inner">
                        <?php
                            $cat_posts = get_posts(
                                array(
                                    'category' => $cat_id, // カテゴリIDを番号で指定する場合
                                    'category_name' => $cat_slug, // カテゴリをスラッグで指定する場合
                                    'has_password' => false, // パスワード保護投稿を除外
                                    'posts_per_page' => 6, // 表示件数
                                )
                            );
                            global $post;
                        ?>
                        <?php if($cat_posts): ?>
                        <ul class="card-list card-list--main" data-card-list>
                            <?php foreach($cat_posts as $post): setup_postdata($post); ?>
                            <li class="card-vertical">
                                <a href="<?php the_permalink() ?>" class="card-vertical__anchor">
                                    <figure class="card-vertical__image-wrap">
                                        <?php if (has_post_thumbnail()): ?>
                                        <?php the_post_thumbnail( 'full', 
                                        array( 
                                            'class' => 'card-vertical__image',
                                            'alt' => '',
                                            'loading' => 'lazy'
                                        ) ); 
                                        ?>
                                        <?php elseif (!has_post_thumbnail() && strpos($path, 'kyujin') ): ?>
                                            <img src="<?php echo THEME_IMAGE ?>job_no-image.png" alt="" class="card-vertical__image" loading="lazy">
                                        <?php else: ?>
                                            <img src="<?php echo THEME_IMAGE ?>no-image.png" alt="" class="card-vertical__image" loading="lazy">
                                        <?php endif; ?>
                                    </figure>
                                    <dl class="card-vertical__summary">
                                    <dt class="card-vertical__term">
                                        <?php echo get_avatar( get_the_author_id(), 96, '', '', $args = array( 'class' => 'card-vertical__avater' ) ); ?>
                                        <span class="card-vertical__title">
                                            <?php the_title(); ?>
                                        </span>
                                    </dt>
                                    <dd class="card-vertical__description">
                                        <span class="card-vertical__author"
                                        ><?php the_author(); ?></span>
                                        <time class="card-vertical__date"><?php the_time('Y.m.d') ?></time>
                                    </dd>
                                    </dl>
                                </a>
                            </li>
                            <?php endforeach; wp_reset_postdata(); ?>
                        </ul>
                            <?php 
                                $count_args = array(
                                    'category' => $cat_id,
                                    'posts_per_page' => -1,
                                    'post_type' => $post_type
                                );
                                //echo count( get_posts( $count_args ) );
                                if(count( get_posts( $count_args ) ) > 6): 
                            ?>
                                <button
                                    type="button"
                                    class="arrow-button arrow-button--bottom"
                                    data-infinite
                                >
                                    <i class="arrow-button__icon" data-infinite-icon></i>
                                    <span class="arrow-button__loading" data-infinite-loading>
                                    <i></i>
                                    <i></i>
                                    <i></i>
                                    <i></i>
                                    <i></i>
                                    <i></i>
                                    <i></i>
                                    <i></i>
                                    </span>
                                </button>
                            <?php endif; ?>

                        <?php else: ?>
                            <!-- 記事なし -->
                            <p class="text">記事はありません</p>
                        <?php endif; ?>

                    </div>
                </section>
            </main>
            <!-- aside_nav -->
            <?php get_template_part('_inc/aside_nav'); ?>
        </div>
    </div>
    <!-- /content -->

    <!-- footer -->
    <?php get_template_part('_inc/footer'); ?>
    <script>
        const categories_id = "<?php $cat_children = get_term_children( $cat_id, 'category' );
                echo $cat_id; foreach ($cat_children as $cat_child) { echo ',' . $cat_child; }
            ?>";
    </script>
    <?php wp_footer(); ?>
</body>
</html>