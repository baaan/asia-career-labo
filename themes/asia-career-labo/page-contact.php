<!DOCTYPE html>
<html lang="ja">
<head>
    <!-- head -->
    <?php get_template_part('_inc/head'); ?>
</head>
<body>
    <!-- header -->
    <?php get_template_part('_inc/header'); ?>
    <!-- global_nav -->
    <?php get_template_part('_inc/global_nav'); ?>

    <!-- content -->
    <div class="wrapper wrapper--pages">
      <div class="wrapper__inner wrapper__inner--pages" data-wrapper>
        <main class="main" data-main>
          <section class="section">
            <h1 class="heading-primary">
              <span class="heading-primary__main">CONTACT</span>
              <span class="heading-primary__sub">お問い合わせ</span>
            </h1>
            <div class="section__inner">
              <form action="" method="" class="form" data-form>
                <div class="form__toaster" data-form-toaster>
                  送信完了しました。<br>自動返信メールをご確認ください。
                  <!-- 入力内容に問題があります。<br>確認して再度お試しください。 -->
                </div>
                <div class="form__row">
                  <p class="form__annotation">* が付いている欄は必須項目です。</p>
                </div>
                <div class="form__row">
                  <div class="form__select-wrap">
                    <select name="your-subject" class="form__select"  data-form-select data-form-require>
                      <option >ご用件をお選びください*</option>
                      <option value="求人内容について問い合わせたい">求人内容について問い合わせたい</option>
                      <option value="就職相談したい">就職相談したい</option>
                      <option value="求人を掲載したい">求人を掲載したい</option>
                      <option value="イベント情報を告知したい">イベント情報を告知したい</option>
                      <option value="自社の記事を書いてほしい">自社の記事を書いてほしい</option>
                      <option value="その他">その他</option>
                    </select>
                  </div>
                </div>
                <div class="form__row">
                  <input type="text" name="your-name" value="" placeholder="お名前*" autocomplete="name" class="form__input" data-form-require>
                </div>
                <div class="form__row">
                  <input type="email" name="your-email" value="" placeholder="メールアドレス*" autocomplete="email" class="form__input" data-form-require>
                </div>
                <div class="form__row">
                  <input type="url" name="your-url" value="" placeholder="サイト" autocomplete="on" class="form__input">
                </div>
                <div class="form__row">
                  <textarea name="your-comment" placeholder="お問い合わせ内容*" class="form__textarea" data-form-require></textarea>
                </div>
                <button type="submit" class="button-primary" disabled data-form-submit>送信</button>
              </form>
              <div class="line-add">
                <p class="line-add__text">LINEでのご連絡をご希望の方はこちら</p>
                <a href="https://line.me/R/ti/p/%40936fgdso" target="_blank" class="line-add__anchor">
                  <img src="<?php echo THEME_IMAGE ?>contact/line_add.png" alt="LINE 友だち追加" class="line-add__image">
                </a>
              </div>
            </div>
          </section>
        </main>
      </div>
    </div>
    <!-- /content -->

    <!-- footer -->
    <?php get_template_part('_inc/footer'); ?>
    <?php wp_footer(); ?>
</body>
</html>