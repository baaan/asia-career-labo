<!DOCTYPE html>
<html lang="ja">
<head>
    <!-- head -->
    <?php get_template_part('_inc/head'); ?>
</head>
<body>
    <!-- header -->
    <?php get_template_part('_inc/header'); ?>
    <!-- global_nav -->
    <?php get_template_part('_inc/global_nav'); ?>

    <!-- content -->
    <div class="wrapper wrapper--form">
      <div class="wrapper__inner" data-wrapper>
        <main class="main" data-main>
          <?php if ( have_posts() ) : ?>
            <?php while( have_posts() ) : the_post(); ?>
            <section class="section section--form">
              <div class="section__inner section__inner--form">
                <div class="post-page">
                    <div class="googleform">
                      <?php the_content(); ?>
                    </div>
                </div>
              </div>
            </section>
            <?php endwhile;?>
          <?php endif; ?>
        </main>
      </div>
    </div>
    <!-- /content -->

    <!-- footer -->
    <?php get_template_part('_inc/footer'); ?>
    <?php wp_footer(); ?>
</body>
</html>