<!DOCTYPE html>
<html lang="ja">
<head>
    <!-- head -->
    <?php get_template_part('_inc/head'); ?>
</head>
<body>
    <!-- header -->
    <?php get_template_part('_inc/header'); ?>
    <!-- global_nav -->
    <?php get_template_part('_inc/global_nav'); ?>

    <!-- content -->
    <div class="wrapper">
      <div class="wrapper__inner wrapper__inner--category" data-wrapper>
        <main class="main" data-main>
          <section class="section">
            <h1 class="heading-primary">
              <span class="heading-primary__main">EVENT</span>
              <span class="heading-primary__sub">イベント情報</span>
            </h1>

            <!-- content -->
            <div class="section__inner">
                <?php
                    $event_posts = get_posts(
                        array(
                            'post_type' => 'event', // 投稿タイプ
                            'posts_per_page' => 8, // 表示件数
                            'has_password' => false, // パスワード保護投稿を除外
                        )
                    );
                    global $post;
                ?>
                <?php if($event_posts): ?>
                <ul class="card-list card-list--event-vertical" data-card-list>
                    <?php foreach($event_posts as $post): setup_postdata($post); ?>
                    <?php 
                        $date = date_create(get_field('event_yymmdd'));
                        $year  = date_format($date,'Y');
                        $month = date_format($date,'m');
                        $day   = date_format($date,'d');
                        $datetime = new DateTime();
                        $datetime -> setDate($year, $month, $day);
                        $week = array("日", "月", "火", "水", "木", "金", "土");
                        $w = (int)$datetime -> format('w');
                    ?>
                    <li class="card-event">
                        <a href="<?php the_permalink() ?>" class="card-event__anchor">
                            <div class="card-event__schedule">
                            <span class="card-event__month"><?php echo date_format($date,'m'); ?></span>
                            <span class="card-event__placeholder-date">
                                <span class="card-event__date"><?php echo date_format($date,'d'); ?></span
                                ><span class="card-event__dateofweek"><?php echo $week[$w]; ?></span>
                            </span>
                            <span class="card-event__placeholder-time">
                                <time class="card-event__start-time"><?php the_field('event_start_time'); ?></time
                                ><time class="card-event__finish-time"><?php the_field('event_finish_time'); ?></time>
                            </span>
                            </div>
                            <figure class="card-event__image-wrap">
                            <?php if (has_post_thumbnail()): ?>
                                <?php the_post_thumbnail( 'thumbnail', 
                                array( 
                                    'class' => 'card-event__image',
                                    'alt' => '',
                                    'loading' => 'lazy'
                                ) ); 
                                ?>
                                <?php else: ?>
                                    <img src="<?php echo THEME_IMAGE ?>/no-image.png" alt="" class="card-event__image" loading="lazy">
                            <?php endif; ?>
                            </figure>
                            <dl class="card-event__sumarry">
                                <dt class="card-event__title"><?php the_title(); ?></dt>
                                <dd class="card-event__place">
                                    <?php the_field('event_place_text') ?>
                                </dd>
                            </dl>
                            <?php if ( get_field('event_close') === true ): ?>
                            <span class="label label--attention">終了しました</span>
                            <?php endif; ?>
                        </a>
                    </li>
                    <?php endforeach; wp_reset_postdata(); ?>
                </ul>
                    <?php
                        $count_event = wp_count_posts('event');
                        $event_posts = $count_event -> publish;
                        if(($event_posts) > 8):
                    ?>
                        <button
                            type="button"
                            class="arrow-button arrow-button--bottom"
                            data-infinite
                        >
                            <i class="arrow-button__icon" data-infinite-icon></i>
                            <span class="arrow-button__loading" data-infinite-loading>
                            <i></i>
                            <i></i>
                            <i></i>
                            <i></i>
                            <i></i>
                            <i></i>
                            <i></i>
                            <i></i>
                            </span>
                        </button>
                    <?php endif; ?>

                <?php else: ?>
                    <!-- イベント情報なし -->
                    <p class="text">イベント情報はありません</p>
                <?php endif; ?>

            </div>
            <!-- /content -->
        </div>
    </div>
    <!-- /content -->

    <!-- footer -->
    <?php get_template_part('_inc/footer'); ?>
    <?php wp_footer(); ?>
</body>
</html>