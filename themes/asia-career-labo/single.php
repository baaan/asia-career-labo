<!DOCTYPE html>
<html lang="ja">
<head>
    <!-- head -->
    <?php get_template_part('_inc/head'); ?>
</head>
<body>
    <!-- header -->
    <?php get_template_part('_inc/header'); ?>
    <!-- global_nav -->
    <?php get_template_part('_inc/global_nav'); ?>

    <!-- content -->
    <div class="wrapper">
        <div class="wrapper__inner wrapper__inner--single" data-wrapper>
            <main class="main" data-main>

                <!-- Hero -->
                <?php get_template_part('_inc/single/hero'); ?>

                <section class="section section--single">

                    <!-- Post start -->
                    <?php if(get_field('post_start')): ?>
                    <div class="post-start">
                        <?php the_field('post_start'); ?>
                    </div>
                    <?php endif; ?>
                     <!-- /Post start -->

                    <!-- Author START -->
                    <?php 
                        $author_id = get_the_author_meta('id');
                        $author_degree = get_field('profile_degree', 'user_'. $author_id);
                        $author_twitter = get_field('profile_twitter', 'user_'. $author_id);
                        $author_instagram = get_field('profile_instagram', 'user_'. $author_id);
                        $author_youtube = get_field('profile_youtube', 'user_'. $author_id);
                        $author_website = get_field('profile_website', 'user_'. $author_id);
                        $author_email = get_field('profile_email', 'user_'. $author_id);
                        $author_introduction_short = get_field('profile_introduction_short', 'user_'. $author_id);
                        $auhtor_name = get_userdata($author_id);
                        $author_avater = get_avatar($author_id, 160, '', '', $args = array( 'class' => 'author__avater', 'loading' => 'lazy' ) );
                    ?>
                    <div class="author author--single">
                        <a href="<?php echo get_author_posts_url( $author_id ); ?>" class="author__avater-wrap">
                            <?php echo $author_avater ?>
                        </a>
                        <div class="author--single__inner">
                            <div class="author--single__placeholder">
                            <div class="author--single__name-wrap">
                                <span class="author__degree"><?php echo $author_degree ?></span>
                                <a href="<?php echo get_author_posts_url( $author_id ); ?>" class="author__name">
                                    <?php the_author_meta( 'display_name', $author_id ); ?>
                                </a>
                            </div>
                            <?php if (!empty($author_twitter) || !empty($author_instagram) || !empty($author_youtube) || !empty($author_website) || !empty($author_email)): ?>
                            <ul class="author__sns">
                                <?php if (!empty($author_twitter)): ?>
                                <li class="author__sns-item">
                                <a href="<?php echo $author_twitter ?>" class="author__sns-anchor" target="_blank">
                                    <svg class="author__sns-icon">
                                    <title>Twitter</title>
                                    <use
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#twitter"
                                    ></use>
                                    </svg>
                                </a>
                                </li>
                                <?php endif; ?>
                                <?php if (!empty($author_instagram)): ?>
                                <li class="author__sns-item">
                                <a href="<?php echo $author_instagram ?>" class="author__sns-anchor" target="_blank">
                                    <svg class="author__sns-icon">
                                    <title>Instagram</title>
                                    <use
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#instagram"
                                    ></use>
                                    </svg>
                                </a>
                                </li>
                                <?php endif; ?>
                                <?php if (!empty($author_youtube)): ?>
                                <li class="author__sns-item">
                                <a href="<?php echo $author_youtube ?>" class="author__sns-anchor" target="_blank">
                                    <svg class="author__sns-icon">
                                    <title>Youtube</title>
                                    <use
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#youtube"
                                    ></use>
                                    </svg>
                                </a>
                                </li>
                                <?php endif; ?>
                                <?php if (!empty($author_website)): ?>
                                <li class="author__sns-item">
                                <a href="<?php echo $author_website ?>" class="author__sns-anchor" target="_blank">
                                    <svg class="author__sns-icon">
                                    <title>Web</title>
                                    <use
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#web"
                                    ></use>
                                    </svg>
                                </a>
                                </li>
                                <?php endif; ?>
                                <?php if (!empty($author_email)): ?>
                                <li class="author__sns-item">
                                <a href="mailto:<?php echo $author_email ?>" class="author__sns-anchor">
                                    <svg class="author__sns-icon">
                                    <title>Mail</title>
                                    <use
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#mail"
                                    ></use>
                                    </svg>
                                </a>
                                </li>
                                <?php endif; ?>
                            </ul>
                            <?php endif; ?>
                            </div>
                            <?php if (!empty($author_introduction_short)): ?>
                            <div class="author__info">
                                <!-- custom field[short] -->
                                <?php echo $author_introduction_short ?>
                                <!-- /custom field[short] -->
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- /Author END -->

                    <!-- Post body -->
                    <?php if ( have_posts() ) : ?>
                        <?php while( have_posts() ) : the_post(); ?>
                        <div class="post-single">
                            <?php the_content(); ?>
                        </div>
                        <?php endwhile;?>
                    <?php endif; ?>

                    <!-- Post close -->
                    <?php if(get_field('post_close')): ?>
                    <div class="post-close">
                        <?php the_field('post_close'); ?>
                    </div>
                    <?php endif; ?>
                    <!-- /Post close -->

                    <!-- Share buttons -->
                    <div class="share">
                        <p class="share__title">SHERE</p>
                        <ul class="share__list">
                            <li class="share__item">
                            <a href="https://twitter.com/share?url=<?php echo get_the_permalink();?>&text=<?php echo get_the_title();?>" target="_blank" rel="nofollow noopener" class="share__anchor">
                                <svg class="share__icon">
                                <title>Twitter</title>
                                <use
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#twitter_circle"
                                ></use>
                                </svg>
                            </a>
                            </li>
                            <li class="share__item">
                            <a href="http://www.facebook.com/share.php?u=<?php echo get_the_permalink(); ?>" target="_blank" rel="nofollow noopener" class="share__anchor">
                                <svg class="share__icon">
                                <title>Facebook</title>
                                <use
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#facebook_circle"
                                ></use>
                                </svg>
                            </a>
                            </li>
                            <li class="share__item">
                            <a href="https://social-plugins.line.me/lineit/share?url=<?php echo get_the_permalink(); ?>" target="_blank" rel="nofollow noopener" class="share__anchor">
                                <svg class="share__icon">
                                <title>LINE</title>
                                <use
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#line_circle"
                                ></use>
                                </svg>
                            </a>
                            </li>
                        </ul>
                    </div>

                    <!-- Prev・Next ボタン -->
                    <div class="button-set">
                        <?php if ( get_previous_post() ): ?>
                            <?php previous_post_link('%link', '<span><i class="button-prev__icon">PREV</i>%title</span>'); ?>
                        <?php else: ?>
                            <a href="" class="button-prev is-disabled">
                                <span>
                                    <i class="button-prev__icon">PREV</i>
                                    記事がありません
                                </span>
                            </a>
                        <?php endif; ?>
                        <?php if ( get_next_post() ): ?>
                            <?php next_post_link('%link', '<span><i class="button-prev__icon">NEXT</i>%title</span>'); ?>
                        <?php else: ?>
                            <a href="" class="button-next is-disabled">
                                <span>
                                    記事がありません
                                    <i class="button-prev__icon">NEXT</i>
                                </span>
                            </a>
                        <?php endif; ?>
                    </div>
                    <!-- /Prev・Next ボタン -->

                </section>
            </main>
            <!-- aside_nav -->
            <?php get_template_part('_inc/aside_nav'); ?>
        </div>
    </div>
    <!-- /content -->

    <!-- footer -->
    <?php get_template_part('_inc/footer'); ?>
    <?php wp_footer(); ?>
</body>
</html>