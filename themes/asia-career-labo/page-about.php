<!DOCTYPE html>
<html lang="ja">
<head>
    <!-- head -->
    <?php get_template_part('_inc/head'); ?>
    <script src="https://www.youtube.com/iframe_api" defer></script>
    <script defer>
        //youtube API
        let ytPlayer;
        function onYouTubeIframeAPIReady() {
            ytPlayer = new YT.Player("youtube", {
                //動画を表示させたいIDを指定
                videoId: "5PyZ4k0nIks", //動画のアドレスの指定
                playerVars: {
                playsinline: 1, // インライン再生を行う
                autoplay: 1, //自動再生を行う
                fs: 0, //全画面表示ボタンを表示しない
                rel: 0, // 再生中の動画と同じチャンネルの関連動画を表示
                controls: 0, // プレーヤー コントロールを表示しない
                modestbranding: 1, // YouTubeロゴの非表示
                iv_load_policy: 3, // アノテーションの非表示
                start: 0, //0秒後から動画がスタート
                },
                events: {
                //　イベント
                onReady: onPlayerReady,
                onStateChange: onPlayerStateChange,
                },
            });
        }

        //ミュートにしてから再生する設定
        function onPlayerReady(event) {
            event.target.mute();
            event.target.playVideo();
        }

        //ループ設定
        function onPlayerStateChange(event) {
            if (event.data == YT.PlayerState.ENDED) {
                event.target.playVideo();
            }
        }
    </script>
</head>
<body>
    <!-- header -->
    <?php get_template_part('_inc/header'); ?>
    <!-- global_nav -->
    <?php get_template_part('_inc/global_nav'); ?>

    <!-- content -->
    <div class="wrapper wrapper--about">
      <div class="slider-about">
        <div id="swiper-hero-about" class="slider-about__inner">
          <ul class="slider-about__list swiper-wrapper" data-slider-about>
            <li class="slider-about__item swiper-slide">
              <figure class="slider-about__image-wrap">
                <img
                  src="<?php echo THEME_IMAGE ?>about/h_01.jpg"
                  alt=""
                  class="slider-about__image"
                />
              </figure>
            </li>
            <li class="slider-about__item swiper-slide">
              <figure class="slider-about__image-wrap">
                <img
                  src="<?php echo THEME_IMAGE ?>about/h_02.jpg"
                  alt=""
                  class="slider-about__image"
                />
              </figure>
            </li>
            <li class="slider-about__item swiper-slide">
              <figure class="slider-about__image-wrap">
                <img
                  src="<?php echo THEME_IMAGE ?>about/h_03.jpg"
                  alt=""
                  class="slider-about__image"
                />
              </figure>
            </li>
          </ul>
        </div>
        <div class="slider-about__catchcopy">
          <h1 class="slider-about__heading">
            海外で<br class="break-sm-and-tb" />生きることを<br />もっと身近に
          </h1>
          <p class="slider-about__text">
            人生やキャリアに悩めるすべての人に
            <br class="break-sm-and-tb" />「海外キャリア」という選択肢を。
          </p>
        </div>
      </div>

      <div class="wrapper__inner wrapper__inner--about" data-wrapper>
        <div class="youtube-bg">
          <div id="youtube" class="youtube-bg__video"></div>
          <div class="youtube-bg__overlay"></div>
        </div>
        <div class="wrapper--about__placeholder">
          <svg class="wrapper--about__logo">
            <title>ASIA CAREER LABO</title>
            <use
              xmlns:xlink="http://www.w3.org/1999/xlink"
              xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#logotype_vertical"
            ></use>
          </svg>
          <main class="main main--about">
            <?php
              $about_lead = get_field('about_lead');
              $about_mission = get_field('about_mission');
              $about_before = get_field('about_before');
              $about_after = get_field('about_after');
              $about_message = get_field('about_message');
            ?>
            <section class="section section--about">
              <div class="about-post-lead">
                <!-- custome field -->
                <?php echo $about_lead; ?>
                <!-- /custome field -->
              </div>
            </section>
            <section class="section section--about">
              <h2 class="heading-secondary heading-secondary--invert">
                <span class="heading-secondary__main">MISSION</span>
                <span class="heading-secondary__sub">アジラボのミッション</span>
              </h2>

              <div class="about-post-mission">
                <!-- custome field -->
                <?php echo $about_mission; ?>
                <!-- /custome field -->
              </div>
            </section>
            <section class="section section--about">
              <div class="about-mission">
                <h3 class="about-mission__heading">
                  こんなお悩みをお持ちの方は、
                  <br />ぜひ一度ご相談ください。
                </h3>
                <dl class="about-baloon">
                  <dt class="about-baloon__term">BEFORE</dt>
                  <dd class="about-baloon__desc">
                    <h4 class="about-baloon__heading">移住前のお悩み</h4>
                    <div class="about-post-before">
                      <!-- custome field -->
                      <?php echo $about_before; ?>
                      <!-- /custome field -->
                    </div>
                  </dd>
                </dl>
                <dl class="about-baloon">
                  <dt class="about-baloon__term">AFTER</dt>
                  <dd class="about-baloon__desc">
                    <h4 class="about-baloon__heading">移住後のお悩み</h4>
                    <div class="about-post-after">
                      <!-- custome field -->
                      <?php echo $about_after; ?>
                      <!-- /custome field -->
                    </div>
                  </dd>
                </dl>
              </div>
            </section>
            <section class="section section--about">
              <h2
                class="
                  heading-secondary
                  heading-secondary--invert
                  heading-secondary--vertical@sm
                "
              >
                <span class="heading-secondary__main">MESSAGE</span>
                <span class="heading-secondary__sub">アジラボの想い</span>
              </h2>

              <div class="about-post-message">
                <!-- custome field -->
                <?php echo $about_message; ?>
                <!-- /custome field -->
              </div>
            </section>
          </main>
        </div>
        <aside class="about-member">
          <div class="about-member__inner">
            <span class="about-member__bg-triangle" data-bg-triangle></span>

            <h2 class="heading-secondary">
              <span class="heading-secondary__main">MEMBER</span>
              <span class="heading-secondary__sub">アジラボメンバー</span>
            </h2>
            <ul class="author-list author-list--card">
              <?php
                // リョータ
                $member_main_001 = get_field('about_main_member_1');
                $author_name = get_user_by( 'id', $member_main_001 ) -> display_name;
                $author_degree = get_field('profile_degree', 'user_'. $member_main_001);
                $author_twitter = get_field('profile_twitter', 'user_'. $member_main_001);
                $author_instagram = get_field('profile_instagram', 'user_'. $member_main_001);
                $author_youtube = get_field('profile_youtube', 'user_'. $member_main_001);
                $author_website = get_field('profile_website', 'user_'. $member_main_001);
                $author_email = get_field('profile_email', 'user_'. $member_main_001);
                $author_introduction_short = get_field('profile_introduction_short', 'user_'. $member_main_001);
                $author_introduction_long = get_field('profile_introduction_long', 'user_'. $member_main_001);
                $author_about_image = get_field('profile_about_image', 'user_'. $member_main_001);
              ?>
              <li class="author-card">
                <figure class="author-card__avater">
                  <a href="<?php echo get_author_posts_url( $member_main_001 ); ?>" class="author__anchor">
                    <img
                      src="<?php echo $author_about_image['url'] ?>"
                      alt=""
                      class="author-card__image"
                      loading="lazy"
                    />
                  </a>
                  <figcaption class="author-card__detail">
                    <span class="author-card__account">
                      <span class="author-card__degree"
                        ><?php echo $author_degree ?></span
                      >
                      <span class="author-card__name">
                        <a href="<?php echo get_author_posts_url( $member_main_001 ); ?>" class="author__anchor">
                          <?php echo $author_name ?>
                        </a>
                      </span>
                    </span>
                    <span class="author-card__sns">
                      <ul class="author-card__list">
                        <?php if (!empty($author_twitter)): ?>
                        <li class="author-card__item">
                          <a href="<?php echo $author_twitter ?>" target="_blank" class="author-card__anchor">
                            <svg class="author-card__icon">
                              <title>Twitter</title>
                              <use
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#twitter"
                              ></use>
                            </svg>
                          </a>
                        </li>
                        <?php endif; ?>
                        <?php if (!empty($author_instagram)): ?>
                        <li class="author-card__item">
                          <a href="<?php echo $author_instagram ?>" target="_blank" class="author-card__anchor">
                            <svg class="author-card__icon">
                              <title>Instagram</title>
                              <use
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#instagram"
                              ></use>
                            </svg>
                          </a>
                        </li>
                        <?php endif; ?>
                        <?php if (!empty($author_youtube)): ?>
                        <li class="author-card__item">
                          <a href="<?php echo $author_youtube ?>" target="_blank" class="author-card__anchor">
                            <svg class="author-card__icon">
                              <title>Youtube</title>
                              <use
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#youtube"
                              ></use>
                            </svg>
                          </a>
                        </li>
                        <?php endif; ?>
                        <?php if (!empty($author_website)): ?>
                        <li class="author-card__item">
                          <a href=<?php echo $author_website ?>" target="_blank" class="author-card__anchor">
                            <svg class="author-card__icon">
                              <title>Web</title>
                              <use
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#web"
                              ></use>
                            </svg>
                          </a>
                        </li>
                        <?php endif; ?>
                        <?php if (!empty($author_email)): ?>
                        <li class="author-card__item">
                          <a href="mailto:<?php echo $author_email ?>" class="author-card__anchor">
                            <svg class="author-card__icon">
                              <title>Mail</title>
                              <use
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#mail"
                              ></use>
                            </svg>
                          </a>
                        </li>
                        <?php endif; ?>
                      </ul>
                    </span>
                  </figcaption>
                </figure>
                <?php if (!empty($author_introduction_short) || !empty($author_introduction_long)): ?>
                <div class="author-card__text">
                  <!-- custom field -->
                  <?php if (!empty($author_introduction_long)): ?>
                    <?php echo $author_introduction_long ?>
                  <?php elseif (!empty($author_introduction_short)): ?>
                    <?php echo $author_introduction_short ?>
                  <?php endif; ?>
                  <!-- /custom field -->
                </div>
                <?php endif; ?>
              </li>
              <?php
                // ひろ
                $member_main_002 = get_field('about_main_member_2');
                $author_name = get_user_by( 'id', $member_main_002 ) -> display_name;
                $author_degree = get_field('profile_degree', 'user_'. $member_main_002);
                $author_twitter = get_field('profile_twitter', 'user_'. $member_main_002);
                $author_instagram = get_field('profile_instagram', 'user_'. $member_main_002);
                $author_youtube = get_field('profile_youtube', 'user_'. $member_main_002);
                $author_website = get_field('profile_website', 'user_'. $member_main_002);
                $author_email = get_field('profile_email', 'user_'. $member_main_002);
                $author_introduction_short = get_field('profile_introduction_short', 'user_'. $member_main_002);
                $author_introduction_long = get_field('profile_introduction_long', 'user_'. $member_main_002);
                $author_about_image = get_field('profile_about_image', 'user_'. $member_main_002);
              ?>
              <li class="author-card">
                <figure class="author-card__avater">
                  <a href="<?php echo get_author_posts_url( $member_main_002 ); ?>" class="author__anchor">
                    <img
                      src="<?php echo $author_about_image['url'] ?>"
                      alt=""
                      class="author-card__image"
                      loading="lazy"
                    />
                  </a>
                  <figcaption class="author-card__detail">
                    <span class="author-card__account">
                      <span class="author-card__degree"
                        ><?php echo $author_degree ?></span
                      >
                      <span class="author-card__name">
                        <a href="<?php echo get_author_posts_url( $member_main_002 ); ?>" class="author__anchor">
                          <?php echo $author_name ?>
                        </a>
                      </span>
                    </span>
                    <span class="author-card__sns">
                      <ul class="author-card__list">
                        <?php if (!empty($author_twitter)): ?>
                        <li class="author-card__item">
                          <a href="<?php echo $author_twitter ?>" target="_blank" class="author-card__anchor">
                            <svg class="author-card__icon">
                              <title>Twitter</title>
                              <use
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#twitter"
                              ></use>
                            </svg>
                          </a>
                        </li>
                        <?php endif; ?>
                        <?php if (!empty($author_instagram)): ?>
                        <li class="author-card__item">
                          <a href="<?php echo $author_instagram ?>" target="_blank" class="author-card__anchor">
                            <svg class="author-card__icon">
                              <title>Instagram</title>
                              <use
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#instagram"
                              ></use>
                            </svg>
                          </a>
                        </li>
                        <?php endif; ?>
                        <?php if (!empty($author_youtube)): ?>
                        <li class="author-card__item">
                          <a href="<?php echo $author_youtube ?>" target="_blank" class="author-card__anchor">
                            <svg class="author-card__icon">
                              <title>Youtube</title>
                              <use
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#youtube"
                              ></use>
                            </svg>
                          </a>
                        </li>
                        <?php endif; ?>
                        <?php if (!empty($author_website)): ?>
                        <li class="author-card__item">
                          <a href=<?php echo $author_website ?>" target="_blank" class="author-card__anchor">
                            <svg class="author-card__icon">
                              <title>Web</title>
                              <use
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#web"
                              ></use>
                            </svg>
                          </a>
                        </li>
                        <?php endif; ?>
                        <?php if (!empty($author_email)): ?>
                        <li class="author-card__item">
                          <a href="mailto:<?php echo $author_email ?>" class="author-card__anchor">
                            <svg class="author-card__icon">
                              <title>Mail</title>
                              <use
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#mail"
                              ></use>
                            </svg>
                          </a>
                        </li>
                        <?php endif; ?>
                      </ul>
                    </span>
                  </figcaption>
                </figure>
                <?php if (!empty($author_introduction_short) || !empty($author_introduction_long)): ?>
                <div class="author-card__text">
                  <!-- custom field -->
                  <?php if (!empty($author_introduction_long)): ?>
                    <?php echo $author_introduction_long ?>
                  <?php elseif (!empty($author_introduction_short)): ?>
                    <?php echo $author_introduction_short ?>
                  <?php endif; ?>
                  <!-- /custom field -->
                </div>
                <?php endif; ?>
              </li>
            </ul>

            <ul class="author-list author-list--vertical@sm">
            <?php
                $member_sub_001 = get_field('about_sub_member_1');
                $member_sub_002 = get_field('about_sub_member_2');
                $member_sub_003 = get_field('about_sub_member_3');
                $users_sub = array((int)$member_sub_001, (int)$member_sub_002, (int)$member_sub_003);
                $args = array(
                  'include' => $users_sub,
                  'orderby' => 'ID',
                  'order' => 'ASC'
                );
                $users = get_users( $args );
              ?>
              <?php foreach($users as $user): ?>
              <?php
                $author_name = $user->display_name;
                $author_degree = get_field('profile_degree', 'user_'. $user -> ID);
                $author_twitter = get_field('profile_twitter', 'user_'. $user -> ID);
                $author_instagram = get_field('profile_instagram', 'user_'. $user -> ID);
                $author_youtube = get_field('profile_youtube', 'user_'. $user -> ID);
                $author_website = get_field('profile_website', 'user_'. $user -> ID);
                $author_email = get_field('profile_email', 'user_'. $user -> ID);
                $author_introduction_short = get_field('profile_introduction_short', 'user_'. $user -> ID);
                $author_introduction_long = get_field('profile_introduction_long', 'user_'. $user -> ID);
                $author_about_image = get_field('profile_about_image', 'user_'. $user -> ID);
              ?>
              <li class="author author--list-item@about">
                <figure class="author__avater-wrap">
                  <a href="<?php echo get_author_posts_url( $user -> ID ); ?>" class="author__anchor">
                    <?php echo get_avatar( $user -> ID, 144, '', '', $args = array( 'class' => 'author__avater', 'loading' => 'lazy' ) ); ?>
                  </a>
                  <?php if (!empty($author_degree)): ?>
                  <figcaption class="author__degree">
                    <?php echo $author_degree; ?>
                  </figcaption>
                  <?php endif; ?>
                  <figcaption class="author__name">
                    <a href="<?php echo get_author_posts_url( $user -> ID ); ?>" class="author__anchor">
                      <?php echo $author_name; ?>
                    </a>
                  </figcaption>
                </figure>
                <?php if (!empty($author_twitter) || !empty($author_instagram) || !empty($author_youtube) || !empty($author_website) || !empty($author_email)): ?>
                <ul class="author__sns">
                  <?php if (!empty($author_twitter)): ?>
                  <li class="author__sns-item">
                    <a href="<?php echo $author_twitter ?>" target="_blank" class="author__sns-anchor">
                      <svg class="author__sns-icon">
                        <title>Twitter</title>
                        <use
                          xmlns:xlink="http://www.w3.org/1999/xlink"
                          xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#twitter"
                        ></use>
                      </svg>
                    </a>
                  </li>
                  <?php endif; ?>
                  <?php if (!empty($author_instagram)): ?>
                  <li class="author__sns-item">
                    <a href="<?php echo $author_instagram ?>" target="_blank" class="author__sns-anchor">
                      <svg class="author__sns-icon">
                        <title>Instagram</title>
                        <use
                          xmlns:xlink="http://www.w3.org/1999/xlink"
                          xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#instagram"
                        ></use>
                      </svg>
                    </a>
                  </li>
                  <?php endif; ?>
                  <?php if (!empty($author_youtube)): ?>
                  <li class="author__sns-item">
                    <a href="<?php echo $author_youtube ?>" target="_blank" class="author__sns-anchor">
                      <svg class="author__sns-icon">
                        <title>Youtube</title>
                        <use
                          xmlns:xlink="http://www.w3.org/1999/xlink"
                          xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#youtube"
                        ></use>
                      </svg>
                    </a>
                  </li>
                  <?php endif; ?>
                  <?php if (!empty($author_website)): ?>
                  <li class="author__sns-item">
                    <a href="<?php echo $author_website ?>" target="_blank" class="author__sns-anchor">
                      <svg class="author__sns-icon">
                        <title>Web</title>
                        <use
                          xmlns:xlink="http://www.w3.org/1999/xlink"
                          xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#web"
                        ></use>
                      </svg>
                    </a>
                  </li>
                  <?php endif; ?>
                  <?php if (!empty($author_email)): ?>
                  <li class="author__sns-item">
                    <a href="mailto:<?php echo $author_email ?>" class="author__sns-anchor">
                      <svg class="author__sns-icon">
                        <title>Mail</title>
                        <use
                          xmlns:xlink="http://www.w3.org/1999/xlink"
                          xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#mail"
                        ></use>
                      </svg>
                    </a>
                  </li>
                  <?php endif; ?>
                </ul>
                <?php endif; ?>
                <?php if (!empty($author_introduction_short)): ?>
                <div class="author__info">
                  <!-- custom field[short] -->
                  <?php echo $author_introduction_short ?>
                  <!-- /custom field[short] -->
                </div>
                <?php endif; ?>
              </li>
              <?php endforeach; ?>
            </ul>
          </div>
        </aside>
      </div>
    </div>
    <!-- /content -->

    <!-- footer -->
    <?php get_template_part('_inc/footer'); ?>
    <?php wp_footer(); ?>
</body>
</html>