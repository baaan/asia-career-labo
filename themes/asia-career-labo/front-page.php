<!DOCTYPE html>
<html lang="ja">
<head>
    <!-- head -->
    <?php get_template_part('_inc/head'); ?>
</head>
<body>
    <!-- splash -->
    <?php get_template_part('_inc/home/splash'); ?>
    <!-- header -->
    <?php get_template_part('_inc/header'); ?>
    <!-- global_nav -->
    <?php get_template_part('_inc/global_nav'); ?>
    <div class="wrapper wrapper--home">
      <div id="swiper-hero" class="slider">
        <?php
            $hero_slides = SCF::get('hero_slides');
        ?>
        <ul class="slider__list swiper-wrapper" data-slider>
          <?php foreach($hero_slides as $slide): ?>
          <?php 
            $label_color = '';
            if ($slide['hero_category'] === '求人情報') {
              $label_color = 'label--green';
            } elseif ($slide['hero_category'] === '就職 / 転職') {
              $label_color = 'label--blue';
            } elseif ($slide['hero_category'] === 'ライフスタイル') {
              $label_color = 'label--pink';
            } elseif ($slide['hero_category'] === '海外移住') {
              $label_color = 'label--violet';
            }
          ?>
          <li class="slider__item swiper-slide">
            <a href="<?php echo $slide['hero_url']; ?>" class="slider__anchor">
              <figure class="slider__image-wrap">
                <img
                  src="<?php echo wp_get_attachment_url($slide['hero_image'], 'full'); ?>"
                  alt=""
                  class="slider__image"
                  loading="eager"
                />
              </figure>
              <div class="slider__card">
                <div class="slider__card-inner">
                  <span class="label <?php echo $label_color; ?>"><?php echo $slide['hero_category']; ?></span>
                  <figure class="slider__avater-wrap">
                    <img
                      src="<?php echo wp_get_attachment_url($slide['hero_avater'], 'full'); ?>"
                      alt=""
                      class="slider__avater"
                    />
                  </figure>
                  <dl class="slider__summary">
                    <dt class="slider__term">
                      <?php echo $slide['hero_title']; ?>
                    </dt>
                    <dd class="slider__description">
                      <span class="slider__author"><?php echo $slide['hero_author']; ?></span>
                      <time class="slider__date"><?php echo $slide['hero_yymmdd']; ?></time>
                    </dd>
                  </dl>
                </div>
              </div>
            </a>
          </li>
          <?php endforeach; ?>
        </ul>
        <?php if (!empty($hero_slides)): ?>
        <div class="arrow-button arrow-button--next">
          <i class="arrow-button__icon"></i>
        </div>
        <div class="arrow-button arrow-button--prev">
          <i class="arrow-button__icon"></i>
        </div>
        <div class="slider__pagination swiper-pagination"></div>
        <?php endif; ?>
      </div>

      <div class="wrapper__inner wrapper__inner--home" data-wrapper>
        <main class="main" data-main>
          <section class="section section--home-latest">
            <h2 class="heading-secondary">
              <span class="heading-secondary__main">LATEST</span>
              <span class="heading-secondary__sub">新着記事</span>
            </h2>
            <?php
                $exclude_id = get_category_by_slug('event') -> term_id;
                $new_posts = get_posts(array(
                    'post_type' => 'post', // 投稿タイプ
                    'cat' => '-' . $exclude_id, //　イベント情報を除外
                    'has_password' => false, // パスワード保護投稿を除外
                    'posts_per_page' => 6, // 表示件数
                ));
                global $post; 
            ?>
            <?php if($new_posts): ?>
            <ul class="card-list card-list--main" data-card-list>
            <?php foreach($new_posts as $post): setup_postdata($post); ?>
                <?php 
                    // ラベル名
                    $catgory_name = get_the_category()[0] -> cat_name;
                    // 大カテゴリラベルカラー
                    $path = (string)get_permalink();
                    if (strpos($path, 'kyujin')) {
                        $label_color = 'label--green';
                    } elseif (strpos($path, 'shushoku')) {
                        $label_color = 'label--blue';
                    } elseif (strpos($path, 'life')) {
                        $label_color = 'label--pink';
                    } elseif (strpos($path, 'migration')) {
                        $label_color = 'label--violet';
                    }
                ?>
                <li class="card-vertical">
                    <a href="<?php the_permalink() ?>" class="card-vertical__anchor">
                        <figure class="card-vertical__image-wrap">
                            <?php if (has_post_thumbnail()): ?>
                            <?php the_post_thumbnail( 'full', 
                            array( 
                                'class' => 'card-vertical__image',
                                'alt' => '',
                                'loading' => 'lazy'
                            ) ); 
                            ?>
                            <?php elseif (!has_post_thumbnail() && strpos($path, 'kyujin') ): ?>
                              <img src="<?php echo THEME_IMAGE ?>job_no-image.png" alt="" class="card-vertical__image" loading="lazy">
                            <?php else: ?>
                                <img src="<?php echo THEME_IMAGE ?>no-image.png" alt="" class="card-vertical__image" loading="lazy">
                            <?php endif; ?>
                            <figcaption class="label <?php echo $label_color ?>">
                                <?php echo $catgory_name ?>
                            </figcaption>
                        </figure>
                        <dl class="card-vertical__summary">
                        <dt class="card-vertical__term">
                            <?php echo get_avatar( get_the_author_id(), 96, '', '', $args = array( 'class' => 'card-vertical__avater' ) ); ?>
                            <span class="card-vertical__title">
                                <?php the_title(); ?>
                            </span>
                        </dt>
                        <dd class="card-vertical__description">
                            <span class="card-vertical__author"
                            ><?php the_author(); ?></span>
                            <time class="card-vertical__date"><?php the_time('Y.m.d') ?></time>
                        </dd>
                        </dl>
                    </a>
                </li>
                <?php endforeach; wp_reset_postdata(); ?>
            </ul>
            <?php else: ?>
                <!-- 記事なし -->
                <p class="text">記事はありません</p>
            <?php endif; ?>

            <a href="<?php echo HOME_URI ?>/category/" class="button-primary">新着記事一覧</a>
          </section>
          <section class="section section--home-author">
            <h2 class="heading-secondary heading-secondary--with-button">
              <span class="heading-secondary__main">WRITER</span>
              <span class="heading-secondary__sub">記事執筆者</span>

              <a href="<?php echo HOME_URI ?>/author/" class="button-primary button-primary--in-heading"
                >すべて見る</a
              >
            </h2>

            <div id="swiper-author" class="author-list-wrap">
              <ul class="author-list author-list--slide swiper-wrapper">
              <!-- リョータ -->
              <?php 
                $user_main_login_001 = get_user_by( 'login', 'Ryota' );
                $user_main_id_001 = $user_main_login_001 -> ID;
                $author_name = $user_main_login_001 -> display_name;
                $author_degree = get_field('profile_degree', 'user_'. $user_main_id_001);
              ?>
              <li class="author author--slide-item swiper-slide">
                <a href="<?php echo get_author_posts_url( $user_main_id_001 ); ?>" class="author__anchor">
                  <figure class="author__avater-wrap">
                    <?php echo get_avatar( $user_main_id_001, 144, '', '', $args = array( 'class' => 'author__avater', 'loading' => 'lazy' ) ); ?>
                    <?php if (!empty($author_degree)): ?>
                    <figcaption class="author__degree">
                      <?php echo $author_degree ?>
                    </figcaption>
                    <?php endif; ?>
                    <figcaption class="author__name">
                      <?php echo $author_name; ?>
                    </figcaption>
                  </figure>
                </a>
              </li>
              <!-- ひろ -->
              <?php 
                $user_main_login_002 = get_user_by( 'login', 'hiro' );
                $user_main_id_002 = $user_main_login_002 -> ID;
                $author_name = $user_main_login_002 -> display_name;
                $author_degree = get_field('profile_degree', 'user_'. $user_main_id_002);
              ?>
              <li class="author author--slide-item swiper-slide">
                <a href="<?php echo get_author_posts_url( $user_main_id_002 ); ?>" class="author__anchor">
                  <figure class="author__avater-wrap">
                    <?php echo get_avatar( $user_main_id_002, 144, '', '', $args = array( 'class' => 'author__avater', 'loading' => 'lazy' ) ); ?>
                    <?php if (!empty($author_degree)): ?>
                    <figcaption class="author__degree">
                      <?php echo $author_degree ?>
                    </figcaption>
                    <?php endif; ?>
                    <figcaption class="author__name">
                      <?php echo $author_name; ?>
                    </figcaption>
                  </figure>
                </a>
              </li>
              <!-- アジラボ編集部 -->
              <?php 
                $user_main_login_003 = get_user_by( 'login', 'henshu' );
                $user_main_id_003 = $user_main_login_003 -> ID;
                $author_name = $user_main_login_003 -> display_name;
                $author_degree = get_field('profile_degree', 'user_'. $user_main_id_003);
              ?>
              <li class="author author--slide-item swiper-slide">
                <a href="<?php echo get_author_posts_url( $user_main_id_003 ); ?>" class="author__anchor">
                  <figure class="author__avater-wrap">
                    <?php echo get_avatar( $user_main_id_003, 144, '', '', $args = array( 'class' => 'author__avater', 'loading' => 'lazy' ) ); ?>
                    <?php if (!empty($author_degree)): ?>
                    <figcaption class="author__degree">
                      <?php echo $author_degree ?>
                    </figcaption>
                    <?php endif; ?>
                    <figcaption class="author__name">
                      <?php echo $author_name; ?>
                    </figcaption>
                  </figure>
                </a>
              </li>
              <?php
                $users = get_users( 
                  array(
                    'orderby' => 'ID',
                    'order' => 'ASC',
                    'exclude' => array($user_main_id_001, $user_main_id_002, $user_main_id_003, 96, 95, 26)
                  )
                );
              ?>
              <?php if($users): ?>
              <?php foreach($users as $user): ?>
                <?php
                  $author_degree = get_field('profile_degree', 'user_'. $user -> ID);
                ?>
                <li class="author author--slide-item swiper-slide">
                  <a href="<?php echo get_author_posts_url( $user -> ID ); ?>" class="author__anchor">
                    <figure class="author__avater-wrap">
                      <?php echo get_avatar( $user -> ID, 144, '', '', $args = array( 'class' => 'author__avater', 'loading' => 'lazy' ) ); ?>
                      <?php if (!empty($author_degree)): ?>
                      <figcaption class="author__degree">
                        <?php echo $author_degree ?>
                      </figcaption>
                      <?php endif; ?>
                      <figcaption class="author__name">
                        <?php echo $user->display_name ; ?>
                      </figcaption>
                    </figure>
                  </a>
                </li>
              <?php endforeach; ?>
              </ul>
              <div class="arrow-button arrow-button--next">
                <i class="arrow-button__icon"></i>
              </div>
              <div class="arrow-button arrow-button--prev">
                <i class="arrow-button__icon"></i>
              </div>
              <?php else: ?>
                  <!-- 著者なし -->
                  <p class="text">存在しません</p>
              <?php endif; ?>
            </div>
          </section>
          <section class="section section--home-event">
            <h2 class="heading-secondary heading-secondary--with-button">
              <span class="heading-secondary__main">EVENT</span>
              <span class="heading-secondary__sub">イベント情報</span>

              <a href="<?php echo HOME_URI ?>/event/" class="button-primary button-primary--in-heading"
                >すべて見る</a
              >
            </h2>

            <div id="swiper-event" class="card-list-wrap">
              <?php
                $event_slides = SCF::get('event_slides');
              ?>
              <?php if (!empty($event_slides)): ?>
              <ul class="card-list card-list--slide swiper-wrapper">
                <?php foreach($event_slides as $slide): ?>
                <?php
                  $date = $slide['event_date_time'];
                  $date_array = explode('.',$date);
                  $year  = $date_array[0];
                  $month = $date_array[1];
                  $day   = $date_array[2];
                  $time = $date_array[3];
                  $datetime = new DateTime();
                  $datetime -> setDate($year, $month, $day);
                  $week = array("日", "月", "火", "水", "木", "金", "土");
                  $w = (int)$datetime -> format('w');
                ?>
                <li class="card-event card-event--slide-item swiper-slide">
                  <a href="<?php echo $slide['event_url']; ?>" class="card-event__anchor">
                    <div class="card-event__schedule">
                      <span class="card-event__month"><?php echo $month; ?></span>
                      <span class="card-event__placeholder-date">
                        <span class="card-event__date"><?php echo $day; ?></span
                        ><span class="card-event__dateofweek"><?php echo $week[$w]; ?></span>
                      </span>
                      <span class="card-event__placeholder-time">
                        <time class="card-event__start-time"><?php echo $time; ?></time
                      </span>
                    </div>
                    <figure class="card-event__image-wrap">
                      <img
                        src="<?php echo wp_get_attachment_url($slide['event_image'], 'full'); ?>"
                        alt=""
                        class="card-event__image"
                        loading="lazy"
                      />
                    </figure>
                    <dl class="card-event__sumarry">
                      <dt class="card-event__title"><?php echo $slide['event_title']; ?></dt>
                      <dd class="card-event__place">
                        <?php echo $slide['event_place']; ?>
                      </dd>
                    </dl>
                    <?php if (!empty($slide['event_close'])): ?>
                    <span class="label label--attention">終了しました</span>
                    <?php endif; ?>
                  </a>
                </li>
                <?php endforeach; ?>
              </ul>
              <div class="arrow-button arrow-button--next">
                <i class="arrow-button__icon"></i>
              </div>
              <div class="arrow-button arrow-button--prev">
                <i class="arrow-button__icon"></i>
              </div>
              <?php else: ?>
              <p class="text">イベント情報はありません</p>
              <?php endif; ?>
            </div>
          </section>
          <section class="section section--home-youtube">
            <h2 class="heading-secondary heading-secondary--invert">
              <span class="heading-secondary__main">YOUTUBE</span>
              <span class="heading-secondary__sub">アジラボチャンネル</span>
            </h2>

            <div id="swiper-youtube" class="youtuve-frame-wrap">
              <?php
                $youtube_slides = SCF::get('youtube_slides');
              ?>
              <?php if (!empty($youtube_slides)): ?>
              <ul class="youtube-frame swiper-wrapper">
                <?php foreach($youtube_slides as $slide): ?>
                <li class="youtube-frame__item swiper-slide">
                  <div class="youtube-frame__video-wrap">
                    <div class="youtube-frame__video">
                      <?php echo $slide['youtube_html']; ?>
                    </div>
                  </div>
                  <a
                    href="<?php echo $slide['youtube_url']; ?>"
                    target="_blank"
                    rel="noopener noreferrer"
                    class="youtube-frame__title"
                  >
                    <?php echo $slide['youtube_title']; ?>
                  </a>
                </li>
                <?php endforeach; ?>
              </ul>
              <div class="arrow-button arrow-button--next">
                <i class="arrow-button__icon"></i>
              </div>
              <div class="arrow-button arrow-button--prev">
                <i class="arrow-button__icon"></i>
              </div>
              <?php else: ?>
              <p class="text text--white">Youtube情報はありません</p>
              <?php endif; ?>
            </div>

            <a
              href="https://www.youtube.com/channel/UC4FiUQwkabHKc7islFWF_7A"
              target="_blank"
              class="
                button-primary button-primary--with-icon button-primary--invert
              "
            >
              <span class="button-primary__text">アジラボチャンネル</span>
              <svg class="button-primary--with-icon__image">
                <title>internal_link</title>
                <use
                  xmlns:xlink="http://www.w3.org/1999/xlink"
                  xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#internal_link"
                ></use>
              </svg>
            </a>
          </section>
        </main>
        <!-- aside_nav -->
        <?php get_template_part('_inc/aside_nav'); ?>
      </div>
    </div>
    <!-- footer -->
    <?php get_template_part('_inc/footer'); ?>
    <?php wp_footer(); ?>
</body>
</html>