<!DOCTYPE html>
<html lang="ja">
<head>
    <!-- head -->
    <?php get_template_part('_inc/head'); ?>
</head>
<body>
    <!-- header -->
    <?php get_template_part('_inc/header'); ?>
    <!-- global_nav -->
    <?php get_template_part('_inc/global_nav'); ?>

    <!-- content -->
    <div class="wrapper wrapper--pages">
      <div class="wrapper__inner wrapper__inner--pages" data-wrapper>
        <main class="main" data-main>
          <section class="section">
          <?php 
            if (is_page('company')) {
              $heading_main = 'COMPANY';
              $heading_sub = '運用会社';
            } elseif (is_page('privacy')) {
              $heading_main = 'PRIVACY POLICY';
              $heading_sub = 'プライバシーポリシー';
            } elseif (is_page('term')) {
              $heading_main = 'TERMS OF SERVICE';
              $heading_sub = '利用規約';
            } else {
              $heading_main = '';
              $heading_sub = '';
            }
          ?>
          <?php if ( have_posts() ) : ?>
            <?php while( have_posts() ) : the_post(); ?>
            <h1 class="heading-primary">
              <span class="heading-primary__main"><?php echo $heading_main ?></span>
              <span class="heading-primary__sub"><?php echo $heading_sub ?></span>
            </h1>
            <div class="section__inner">
              <div class="post-page">
                  <?php the_content(); ?>
              </div>
            </div>
            <?php endwhile;?>
          <?php endif; ?>
          </section>
        </main>
      </div>
    </div>
    <!-- /content -->

    <!-- footer -->
    <?php get_template_part('_inc/footer'); ?>
    <?php wp_footer(); ?>
</body>
</html>