<!DOCTYPE html>
<html lang="ja">
<head>
    <!-- head -->
    <?php get_template_part('_inc/head'); ?>
</head>
<body>
    <!-- header -->
    <?php get_template_part('_inc/header'); ?>
    <!-- global_nav -->
    <?php get_template_part('_inc/global_nav'); ?>

    <!-- content -->
    <div class="wrapper wrapper--tag-posts">
        <div class="wrapper__inner wrapper__inner--tag" data-wrapper>
            <main class="main" data-main>
                <section class="section">
                <h1 class="heading-primary heading-primary--tag-page">
                <span class="heading-primary__sub">TAG</span>
                <span class="heading-primary__main"><?php single_tag_title(); ?></span>
                </h1>
                <?php 
                    // 現在のグローバルクエリを取得
                    global $wp_query;
                    // 直前のクエリーオブジェクトを取得
                    $tag_obj = $wp_query -> get_queried_object();
                    // 現在表示中のタグ名
                    $tag_name = $tag_obj -> name;
                    // 現在表示中のタグID
                    $tag_id = $tag_obj -> term_id;
                    // 現在表示中のタグのスラッグ
                    $tag_slug = $tag_obj -> slug;
                ?>
                <div class="section__inner section__inner--tag">
                    <?php
                        $exclude_id = get_category_by_slug('event') -> term_id;
                        $tag_posts = get_posts(array(
                            'post_type' => 'post', // 投稿タイプ
                            'cat' => '-' . $exclude_id, //　イベント情報を除外
                            'has_password' => false, // パスワード保護投稿を除外
                            'tag_id' => $tag_id, // タグIDを番号で指定する場合
                            'tag'    => $tag_slug, // タグをスラッグで指定する場合
                            'posts_per_page' => 6, // 表示件数
                        ));
                        global $post; 
                    ?>
                    <?php if($tag_posts): ?>
                    <ul class="card-list card-list--main" data-card-list>
                        <?php foreach($tag_posts as $post): setup_postdata($post); ?>
                        <?php 
                                // ラベル名
                                $catgory_name = get_the_category()[0] -> cat_name;
                                // 大カテゴリラベルカラー
                                $path = (string)get_permalink();
                                if (strpos($path, 'kyujin')) {
                                    $label_color = 'label--green';
                                } elseif (strpos($path, 'shushoku')) {
                                    $label_color = 'label--blue';
                                } elseif (strpos($path, 'life')) {
                                    $label_color = 'label--pink';
                                } elseif (strpos($path, 'migration')) {
                                    $label_color = 'label--violet';
                                }
                        ?>
                        <li class="card-vertical">
                            <a href="<?php the_permalink(); ?>" class="card-vertical__anchor">
                                <figure class="card-vertical__image-wrap">
                                    <?php if (has_post_thumbnail()): ?>
                                    <?php the_post_thumbnail( 'full', 
                                        array( 
                                            'class' => 'card-vertical__image',
                                            'alt' => '',
                                            'loading' => 'lazy'
                                        ) ); 
                                    ?>
                                    <?php else: ?>
                                        <img src="<?php echo THEME_IMAGE ?>/no-image.png" alt="" class="card-vertical__image" loading="lazy">
                                    <?php endif; ?>
                                    <figcaption class="label <?php echo $label_color ?>">
                                            <?php echo $catgory_name ?>
                                    </figcaption>
                                </figure>
                                <dl class="card-vertical__summary">
                                    <dt class="card-vertical__term">
                                        <?php echo get_avatar( get_the_author_id(), 96, '', '', $args = array( 'class' => 'card-vertical__avater' ) ); ?>
                                        <span class="card-vertical__title">
                                            <?php the_title(); ?>
                                        </span>
                                    </dt>
                                    <dd class="card-vertical__description">
                                        <span class="card-vertical__author">
                                            <?php the_author(); ?>
                                        </span>
                                        <time class="card-vertical__date"><?php the_time('Y.m.d') ?></time>
                                    </dd>
                                </dl>
                            </a>
                        </li>
                        <?php endforeach; wp_reset_postdata(); ?>
                    </ul>

                        <?php if(get_queried_object() -> count > 6): ?>
                            <button
                                type="button"
                                class="arrow-button arrow-button--bottom"
                                data-infinite
                            >
                                <i class="arrow-button__icon" data-infinite-icon></i>
                                <span class="arrow-button__loading" data-infinite-loading>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                <i></i>
                                </span>
                            </button>
                        <?php endif; ?>

                    <?php else: ?>
                        <!-- 記事なし -->
                        <p class="text">記事はありません</p>
                    <?php endif; ?>

                <div class="tag-box">
                    <h2 class="heading-secondary">
                    <span class="heading-secondary__main">TAG</span>
                    <span class="heading-secondary__sub">タグ一覧</span>
                    </h2>
                    <!-- tag -->
                    <div class="tag-list">
                        <?php
                            $tags = get_tags();
                            foreach( $tags as $tag) {
                                echo '<a href="'. get_tag_link($tag->term_id) . '"' . 'class="tag tag--bg-white">' . $tag->name . '</a>';
                            }
                        ?>
                    </div>
                </div>
                </div>
            </section>
            </main>
            <!-- aside_nav -->
            <?php get_template_part('_inc/aside_nav'); ?>
        </div>
    </div>
    <!-- /content -->

    <!-- footer -->
    <?php get_template_part('_inc/footer'); ?>
    <script>
        const tag_id = <?php echo $tag_id ?>;
    </script>
    <?php wp_footer(); ?>
</body>
</html>