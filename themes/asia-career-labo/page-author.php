<!DOCTYPE html>
<html lang="ja">
<head>
    <!-- head -->
    <?php get_template_part('_inc/head'); ?>
</head>
<body>
    <!-- header -->
    <?php get_template_part('_inc/header'); ?>
    <!-- global_nav -->
    <?php get_template_part('_inc/global_nav'); ?>

    <!-- content -->
    <div class="wrapper">
      <div class="wrapper__inner" data-wrapper>
        <main class="main main--author" data-main>
          <section class="section">
            <h1 class="heading-primary">
              <span class="heading-primary__main">WRITER</span>
              <span class="heading-primary__sub">アジラボライター</span>
            </h1>

            <div class="section__inner">
              <ul class="author-list author-list--author">
                <!-- リョータ -->
                <?php 
                  $user_main_login_001 = get_user_by( 'login', 'Ryota' );
                  $user_main_id_001 = $user_main_login_001 -> ID;
                  $author_name = $user_main_login_001 -> display_name;
                  $author_degree = get_field('profile_degree', 'user_'. $user_main_id_001);
                ?>
                <li class="author author--list-item">
                  <a
                    href="javascript:void(0)"
                    class="author__anchor"
                    data-micromodal-trigger="modal-<?php echo $user_main_id_001 ?>"
                  >
                    <figure class="author__avater-wrap">
                    <?php echo get_avatar( $user_main_id_001, 144, '', '', $args = array( 'class' => 'author__avater', 'loading' => 'eager' ) ); ?>
                      <?php if (!empty($author_degree)): ?>
                      <figcaption class="author__degree">
                        <?php echo $author_degree; ?>
                      </figcaption>
                      <?php endif; ?>
                      <figcaption class="author__name">
                        <?php echo $author_name; ?>
                      </figcaption>
                    </figure>
                  </a>
                </li>
                <!-- ひろ -->
                <?php 
                  $user_main_login_002 = get_user_by( 'login', 'hiro' );
                  $user_main_id_002 = $user_main_login_002 -> ID;
                  $author_name = $user_main_login_002 -> display_name;
                  $author_degree = get_field('profile_degree', 'user_'. $user_main_id_002);
                ?>
                <li class="author author--list-item">
                  <a
                    href="javascript:void(0)"
                    class="author__anchor"
                    data-micromodal-trigger="modal-<?php echo $user_main_id_002 ?>"
                  >
                    <figure class="author__avater-wrap">
                    <?php echo get_avatar( $user_main_id_002, 144, '', '', $args = array( 'class' => 'author__avater', 'loading' => 'eager' ) ); ?>
                      <?php if (!empty($author_degree)): ?>
                      <figcaption class="author__degree">
                        <?php echo $author_degree; ?>
                      </figcaption>
                      <?php endif; ?>
                      <figcaption class="author__name">
                        <?php echo $author_name; ?>
                      </figcaption>
                    </figure>
                  </a>
                </li>
                <!-- アジラボ編集部 -->
                <?php 
                  $user_main_login_003 = get_user_by( 'login', 'henshu' );
                  $user_main_id_003 = $user_main_login_003 -> ID;
                  $author_name = $user_main_login_003 -> display_name;
                  $author_degree = get_field('profile_degree', 'user_'. $user_main_id_003);
                ?>
                <li class="author author--list-item">
                  <a
                    href="javascript:void(0)"
                    class="author__anchor"
                    data-micromodal-trigger="modal-<?php echo $user_main_id_003 ?>"
                  >
                    <figure class="author__avater-wrap">
                    <?php echo get_avatar( $user_main_id_003, 144, '', '', $args = array( 'class' => 'author__avater', 'loading' => 'eager' ) ); ?>
                      <?php if (!empty($author_degree)): ?>
                      <figcaption class="author__degree">
                        <?php echo $author_degree; ?>
                      </figcaption>
                      <?php endif; ?>
                      <figcaption class="author__name">
                        <?php echo $author_name; ?>
                      </figcaption>
                    </figure>
                  </a>
                </li>
                <?php
                  $users = get_users( 
                    array(
                      'orderby' => 'ID',
                      'order' => 'ASC',
                      'exclude' => array($user_main_id_001, $user_main_id_002, $user_main_id_003, 96, 95, 26)
                    )
                  );
                ?>
                <?php if($users): ?>
                <?php foreach($users as $user): ?>
                <?php
                  $author_name = $user->display_name;
                  $author_degree = get_field('profile_degree', 'user_'. $user -> ID);
                ?>
                <li class="author author--list-item">
                  <a
                    href="javascript:void(0)"
                    class="author__anchor"
                    data-micromodal-trigger="modal-<?php echo $user -> ID ?>"
                  >
                    <figure class="author__avater-wrap">
                    <?php echo get_avatar( $user -> ID, 144, '', '', $args = array( 'class' => 'author__avater', 'loading' => 'eager' ) ); ?>
                      <?php if (!empty($author_degree)): ?>
                      <figcaption class="author__degree">
                        <?php echo $author_degree; ?>
                      </figcaption>
                      <?php endif; ?>
                      <figcaption class="author__name">
                        <?php echo $author_name; ?>
                      </figcaption>
                    </figure>
                  </a>
                </li>
                <?php endforeach; ?>
              </ul>
              <?php else: ?>
                  <!-- 著者なし -->
                  <p class="text">存在しません</p>
              <?php endif; ?>
            </div>
          </section>
        </main>
      </div>
    </div>
    <!-- /content -->

    <!-- modal -->
    <?php if($users): ?>
    <?php
      $users = get_users( 
        array(
          'orderby' => 'ID',
          'order' => 'ASC',
          'exclude' => array(96, 95, 26)
        )
      );
    ?>
    <?php foreach($users as $user): ?>
    <?php
      $author_name = $user->display_name;
      $author_degree = get_field('profile_degree', 'user_'. $user -> ID);
      $author_twitter = get_field('profile_twitter', 'user_'. $user -> ID);
      $author_instagram = get_field('profile_instagram', 'user_'. $user -> ID);
      $author_youtube = get_field('profile_youtube', 'user_'. $user -> ID);
      $author_website = get_field('profile_website', 'user_'. $user -> ID);
      $author_email = get_field('profile_email', 'user_'. $user -> ID);
      $author_introduction_short = get_field('profile_introduction_short', 'user_'. $user -> ID);
      $author_others = get_field('profile_others', 'user_'. $user -> ID);
    ?>
    <div id="modal-<?php echo $user -> ID ?>" class="modal" aria-hidden="true">
      <div class="overlay" data-micromodal-close>
        <div class="modal__content" data-modal-content>
          <button type="button" class="close-button" data-micromodal-close>
            <i class="close-button__icon"></i>
          </button>
          <div class="modal__inner">
            <div class="author author--in-modal">
              <figure class="author__avater-wrap">
                <?php echo get_avatar( $user -> ID, 144, '', '', $args = array( 'class' => 'author__avater', 'loading' => 'lazy' ) ); ?>
                <?php if (!empty($author_degree)): ?>
                <figcaption class="author__degree">
                  <?php echo $author_degree; ?>
                </figcaption>
                <?php endif; ?>
                <figcaption class="author__name"><?php echo $author_name; ?></figcaption>
              </figure>
              <?php if (!empty($author_twitter) || !empty($author_instagram) || !empty($author_youtube) || !empty($author_website) || !empty($author_email)): ?>
              <!-- custom fields for user setting -->
              <ul class="author__sns">
                <?php if (!empty($author_twitter)): ?>
                <li class="author__sns-item">
                  <a href="<?php echo $author_twitter ?>" target="_blank" class="author__sns-anchor">
                    <svg class="author__sns-icon">
                      <title>Twitter</title>
                      <use
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#twitter"
                      ></use>
                    </svg>
                  </a>
                </li>
                <?php endif; ?>
                <?php if (!empty($author_instagram)): ?>
                <li class="author__sns-item">
                  <a href="<?php echo $author_instagram ?>" target="_blank" class="author__sns-anchor">
                    <svg class="author__sns-icon">
                      <title>Instagram</title>
                      <use
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#instagram"
                      ></use>
                    </svg>
                  </a>
                </li>
                <?php endif; ?>
                <?php if (!empty($author_youtube)): ?>
                <li class="author__sns-item">
                  <a href="<?php echo $author_youtube ?>" target="_blank" class="author__sns-anchor">
                    <svg class="author__sns-icon">
                      <title>Youtube</title>
                      <use
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#youtube"
                      ></use>
                    </svg>
                  </a>
                </li>
                <?php endif; ?>
                <?php if (!empty($author_website)): ?>
                <li class="author__sns-item">
                  <a href="<?php echo $author_website ?>" target="_blank" class="author__sns-anchor">
                    <svg class="author__sns-icon">
                      <title>Web</title>
                      <use
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#web"
                      ></use>
                    </svg>
                  </a>
                </li>
                <?php endif; ?>
                <?php if (!empty($author_email)): ?>
                <li class="author__sns-item">
                  <a href="mailto:<?php echo $author_email ?>" class="author__sns-anchor">
                    <svg class="author__sns-icon">
                      <title>Mail</title>
                      <use
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#mail"
                      ></use>
                    </svg>
                  </a>
                </li>
                <?php endif; ?>
              </ul>
              <?php endif; ?>
              <?php if (!empty($author_introduction_short)): ?>
              <div class="author__info">
                <!-- custom field[short] -->
                <?php echo $author_introduction_short ?>
                <!-- /custom field[short] -->
              </div>
              <?php endif; ?>
              <?php if (!empty($author_others)): ?>
              <div class="author__info">
                <!-- custom field[other] -->
                <?php echo $author_others ?>
                <!-- /custom fields[other] -->
              </div>
              <!-- /custom fields for user setting -->
              <?php endif; ?>
            </div>
            <a href="<?php echo get_author_posts_url( $user -> ID ); ?>" class="button-primary">記事を読む</a>
          </div>
        </div>
      </div>
    </div>
    <?php endforeach; ?>
    <?php endif; ?>
    <!-- /modal -->

    <!-- footer -->
    <?php get_template_part('_inc/footer'); ?>
    <?php wp_footer(); ?>
</body>
</html>