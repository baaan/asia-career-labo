<?php
/**
 * asia-career-labo Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package asia-career-labo
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'HOME_URI', home_url() );
define( 'THEME_URI', get_template_directory_uri() );
define( 'THEME_IMAGE', THEME_URI . '/img/' );
define( 'THEME_CSS', THEME_URI . '/css/' );
define( 'THEME_JS', THEME_URI . '/js/' );

/**
 * Eyecatch image enable
 */
add_theme_support( 'post-thumbnails', array('post','event') );

// No thumbnail
function thumbnail_check( $post_id, $size='post-thumbnails' ) {
    if ( has_post_thumbnail() ){
      $thumb = get_the_post_thumbnail( $post_id, $size );
    } else {
      $thumb = '<img src="' . THEME_IMAGE . 'no-image.png" alt="">';
    }
    echo $thumb;
}

/* PRE_GET_POSTS */
function customize_main_query ( $query ) {
  if ( ! is_admin() || $query->is_main_query() ) { //管理画面以外 かつ メインクエリー
    if ( $query->is_archive() ) {
      $query->set( 'has_password', false );
    }
  }
}
add_action( 'pre_get_posts', 'customize_main_query' ); // PRE_GET_POSTSにフック


/**
 * WordPress Popular Posts
 */
/**
 * Parses custom content tags in WordPress Popular Posts.
 *
 * @param  string  $html    The HTML markup from the plugin.
 * @param  integer $post_id The post/page ID.
 * @return string
 */
function wpp_parse_custom_avatar_tag( $html, $post_id ) {
  if ( false !== strpos( $html, '{avatar}' ) ) {
      $author_id = get_post_field( 'post_author', $post_id );

      if ( $author_id ) {
          $avatar = get_avatar( $author_id, 96, '', '', $args = array( 'class' => 'card-horizontal__avater' ) );
          $html = str_replace( '{avatar}', $avatar, $html );
      }
  }

  return $html;
}
add_filter( 'wpp_parse_custom_content_tags', 'wpp_parse_custom_avatar_tag', 10, 2 );

function wpp_parse_custom_text_author_tag( $html, $post_id ) {
  if ( false !== strpos( $html, '{text_author}' ) ) {
      $author_id = get_post_field( 'post_author', $post_id );

      if ( $author_id ) {
          $author = get_the_author_meta('display_name', $author_id);;
          $html = str_replace( '{text_author}', $author, $html );
      }
  }

  return $html;
}
add_filter( 'wpp_parse_custom_content_tags', 'wpp_parse_custom_text_author_tag', 10, 2 );

/**
 * wp_head整理
 */
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'wlwmanifest_link');
add_theme_support('automatic-feed-links');
function my_delete_plugin_files() {
  wp_dequeue_style('wp-block-library');
  // wp_dequeue_style('admin-bar');
}
add_action( 'wp_enqueue_scripts', 'my_delete_plugin_files' );
function twpp_deregister_scripts() {
  wp_deregister_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'twpp_deregister_scripts' );

/**
 * css/js
 */
function my_styles()  {
  // トップ
  if ( is_home() || is_front_page() && !is_paged() ) {
    wp_enqueue_style( 'asilabo-home', THEME_CSS . 'home.css', array(), '1.0.0' );
    wp_enqueue_script( 'asilabo-home', THEME_JS . 'home.js', array(), '1.0.0', true );
  // 固定ページ: アジラボとは
  } else if ( is_page('about') ) {
    wp_enqueue_style( 'asilabo-about', THEME_CSS . 'about.css', array(), '1.0.0' );
    wp_enqueue_script( 'asilabo-about', THEME_JS . 'about.js', array(), '1.0.0', true );
  // 固定ページ: お問い合わせ
  } else if ( is_page('contact') ) {
    wp_enqueue_style( 'asilabo-contact', THEME_CSS . 'contact.css', array(), '1.0.0' );
    wp_enqueue_script( 'asilabo-contact', THEME_JS . 'contact.js', array(), '1.0.0', true );
  // 固定ページ: ライター
  } else if ( is_page('author') ) {
    wp_enqueue_style( 'asilabo-author', THEME_CSS . 'author.css', array(), '1.0.0' );
    wp_enqueue_script( 'asilabo-author', THEME_JS . 'author.js', array(), '1.0.0', true );
  // 固定ページ: 新着記事
  } else if ( is_page('category') ) {
    wp_enqueue_style( 'asilabo-category', THEME_CSS . 'category.css', array(), '1.0.0' );
    wp_enqueue_script( 'asilabo-category', THEME_JS . 'category.js', array(), '1.0.0', true );
  // 固定ページ: イベント情報
  } else if ( is_page('event') ) {
    wp_enqueue_style( 'asilabo-category', THEME_CSS . 'event.css', array(), '1.0.0' );
    wp_enqueue_script( 'asilabo-category', THEME_JS . 'event.js', array(), '1.0.0', true );
  // 固定ページ
  } else if ( is_page() ) {
    wp_enqueue_style( 'asilabo-page', THEME_CSS . 'page.css', array(), '1.0.0' );
    wp_enqueue_script( 'asilabo-page', THEME_JS . 'page.js', array(), '1.0.0', true );
  // Googleフォームページ
  } else if ( is_singular('form') ) {
    wp_enqueue_style( 'asilabo-form', THEME_CSS . 'form.css', array(), '1.0.0' );
    wp_enqueue_script( 'asilabo-form', THEME_JS . 'form.js', array(), '1.0.0', true );
  // 記事ページ
  } else if ( is_single() ) {
    wp_enqueue_style( 'asilabo-single', THEME_CSS . 'single.css', array(), '1.0.0' );
    wp_enqueue_script( 'asilabo-single', THEME_JS . 'single.js', array(), '1.0.0', true );
  // 記事一覧: カテゴリ
  } else if ( is_category() || is_author() ) {
    wp_enqueue_style( 'asilabo-category', THEME_CSS . 'category.css', array(), '1.0.0' );
    wp_enqueue_script( 'asilabo-category', THEME_JS . 'category.js', array(), '1.0.0', true );
  // 記事一覧: タグ
  } else if ( is_tag() ) {
    wp_enqueue_style( 'asilabo-tag', THEME_CSS . 'tag.css', array(), '1.0.0' );
    wp_enqueue_script( 'asilabo-tag', THEME_JS . 'tag.js', array(), '1.0.0', true );
  // 404
  } else if ( is_404() ) {
    wp_enqueue_style( 'asilabo-404', THEME_CSS . '404.css', array(), '1.0.0' );
    wp_enqueue_script( 'asilabo-404', THEME_JS . '404.js', array(), '1.0.0', true );
  } else {
    wp_enqueue_style( 'asilabo-base', THEME_CSS . 'base.css', array(), '1.0.0' );
    wp_enqueue_script( 'asilabo-base', THEME_JS . 'base.js', array(), '1.0.0', true );
  }
}
add_action( 'wp_enqueue_scripts', 'my_styles' );

/**
 * Short Code
 */
/* 関連記事 */
function related_post_func($atts) {

  ob_start();

  //post_idが省略された場合のデフォルト値設定
  $a = shortcode_atts( array(
    'post_id' => '1'
  ), $atts, 'related_post');

  //【値取得】
  //ショートコードのpost_idを取得
  $id = $a['post_id'];
  //post_idから投稿情報取得
  $post = get_post($id);
  //投稿タイトル取得
  $title = $post->post_title;
  //パーマリンク取得
  $permalink = get_permalink($id);
  //アイキャッチ画像のURL取得
  $image = wp_get_attachment_url(get_post_thumbnail_id($id));
  //アバター取得
  $avater = get_avatar( get_userdata($post -> post_author) -> ID, 96, '', '', $args = array( 'class' => 'related__avater' ) );
  //ライター名取得
  $author = get_userdata($post -> post_author) -> display_name;
  //投稿年月日取得
  $date = get_the_time('Y.m.d', $id);
  //出力コード
  echo '<a href="' . $permalink . '" class="related">';
    echo '<span class="related__label">関連記事</span>';
    echo '<img src="' . $image . '" alt="' . $title . '" class="related__image" >';
    echo '<div class="related__detail">';
      echo '<p class="related__title">' . $title . '</p>';
      echo '<span class="related__author-wrap">';
        echo $avater;
        echo '<span class="related__author">' . $author . '</span>';
        echo '<span class="related__date">' . $date . '</span>';
      echo '</span>';
    echo '</div>';
  echo '</a>';

  return ob_get_clean();
}
add_shortcode('related_post', 'related_post_func');

/**
 * Prev・Next ボタン
 */
function add_prev_post_link_class($output) {
  return str_replace('<a href=', '<a class="button-prev" href=', $output);
}
add_filter( 'previous_post_link', 'add_prev_post_link_class' );

function add_next_post_link_class($output) {
  return str_replace('<a href=', '<a class="button-next" href=', $output);
}
add_filter( 'next_post_link', 'add_next_post_link_class' );


/**
 * REST API
 */
/* カテゴリ名を取得する関数を登録 */
add_action( 'rest_api_init', 'register_category_name' );
function register_category_name() {
//register_rest_field関数を用いget_category_name関数からカテゴリ名を取得し、追加する
    register_rest_field( 'post',
        'category_name',
        array(
            'get_callback' => 'get_category_name'
        )
    );
}
//$objectは現在の投稿の詳細データが入る
function get_category_name( $object ) {
    $category = get_the_category($object[ 'id' ]);
    $cat_name = $category[0]->cat_name;
    return $cat_name;
}

/* イベント情報 */
register_post_type('event', [
  'show_in_rest' => true,
  'rest_base' => 'event',
]);

/* Googleフォーム */
register_post_type('form', [
  'show_in_rest' => false,
  'rest_base' => 'form',
]);

/* アイキャッチ追加 */
add_action('rest_api_init', 'custom_wp_rest_api_post');
function custom_wp_rest_api_post() {
	register_rest_field(['post', 'event'],
	'featured_image',
	array(
		'get_callback' => 'rest_api_add_image',
		'update_callback' => null,
		'schema' => null,
    )
  );
}

function rest_api_add_image($object, $field_name, $request) {
	$feat_img_array = wp_get_attachment_image_src($object['featured_media'], 'full', true);
	return [
		'src' => $feat_img_array[0],
		'width' => $feat_img_array[1],
		'height' => $feat_img_array[2],
	];
}


/* イベント記事取得 */
add_action( 'rest_api_init', 'add_custom_fields_to_rest' );
function add_custom_fields_to_rest() {
  register_rest_field('post',
    'custom_fields',
    [
      'get_callback'    => 'get_custom_fields_value', // カスタム関数名指定
      'update_callback' => null,
      'schema'          => null,
    ]
  );
}
function get_custom_fields_value() {
	return get_post_custom();
}

// カスタム投稿タイプの追加
add_action( 'init', 'create_post_type' );
function create_post_type() {
  register_post_type( 'event', // 投稿タイプ名の定義
  array(
    'labels' => array(
    'name' => __( 'イベント情報' ), // 表示する投稿タイプ名
    'singular_name' => __( 'イベント情報' )
  ),
  'public' => true,
  'menu_position' =>5,
  'show_in_rest' => true,
  'supports' => array(
    'title',
    'editor',
    'thumbnail',
    'custom-fields',
  ),
  )
);
  register_post_type( 'form', // 投稿タイプ名の定義
  array(
    'labels' => array(
    'name' => __( 'Googleフォーム' ), // 表示する投稿タイプ名
    'singular_name' => __( 'Googleフォーム' )
  ),
  'public' => true,
  'menu_position' =>6,
  'show_in_rest' => true,
  'supports' => array(
    'title',
    'editor',
    'thumbnail',
    'custom-fields',
  ),
  )
);
}

// 新規カスタム投稿タイプ反映の為のリセット
// global $wp_rewrite;
// $wp_rewrite->flush_rules();

?>
