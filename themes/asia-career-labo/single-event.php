<!DOCTYPE html>
<html lang="ja">
<head>
    <!-- head -->
    <?php get_template_part('_inc/head'); ?>
</head>
<body>
    <!-- header -->
    <?php get_template_part('_inc/header'); ?>
    <!-- global_nav -->
    <?php get_template_part('_inc/global_nav'); ?>

    <!-- content -->
    <div class="wrapper">
        <div class="wrapper__inner wrapper__inner--single" data-wrapper>
            <main class="main" data-main>

                <!-- Hero -->
                <?php get_template_part('_inc/single/hero'); ?>

                <section class="section section--single">

                    <!-- Post start -->
                    <?php if(get_field('post_start')): ?>
                    <div class="post-start">
                        <?php the_field('post_start'); ?>
                    </div>
                    <?php endif; ?>
                     <!-- /Post start -->

                    <?php
                        $iframe = get_field('event_place_map');
                        if( !empty($iframe) ): 
                    ?>
                    <!-- Google Map START -->
                    <figure class="post-map">
                        <?php 
                            $pattern = array('/width="\d+"\s/', '/height="\d+"\s/');
                            $replace = array('width="720"', 'height="405"');
                            $result = preg_replace($pattern, $replace, $iframe);
                            echo $result;
                        ?>
                    </figure>
                    <!-- /Google Map END -->
                    <?php endif; ?>

                    <!-- Post body -->
                    <?php if ( have_posts() ) : ?>
                        <?php while( have_posts() ) : the_post(); ?>
                        <div class="post-single">
                            <?php the_content(); ?>
                        </div>
                        <?php endwhile;?>
                    <?php endif; ?>

                    <!-- Post close -->
                    <?php if(get_field('post_close')): ?>
                    <div class="post-close">
                        <?php the_field('post_close'); ?>
                    </div>
                    <?php endif; ?>
                    <!-- /Post close -->

                    <!-- Share buttons -->
                    <div class="share">
                        <p class="share__title">SHERE</p>
                        <ul class="share__list">
                            <li class="share__item">
                            <a href="https://twitter.com/share?url=<?php echo get_the_permalink();?>&text=<?php echo get_the_title();?>" target="_blank" rel="nofollow noopener" class="share__anchor">
                                <svg class="share__icon">
                                <title>Twitter</title>
                                <use
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#twitter_circle"
                                ></use>
                                </svg>
                            </a>
                            </li>
                            <li class="share__item">
                            <a href="http://www.facebook.com/share.php?u=<?php echo get_the_permalink(); ?>" target="_blank" rel="nofollow noopener" class="share__anchor">
                                <svg class="share__icon">
                                <title>Facebook</title>
                                <use
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#facebook_circle"
                                ></use>
                                </svg>
                            </a>
                            </li>
                            <li class="share__item">
                            <a href="https://social-plugins.line.me/lineit/share?url=<?php echo get_the_permalink(); ?>" target="_blank" rel="nofollow noopener" class="share__anchor">
                                <svg class="share__icon">
                                <title>LINE</title>
                                <use
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    xlink:href="<?php echo THEME_IMAGE ?>icons_sprite.svg#line_circle"
                                ></use>
                                </svg>
                            </a>
                            </li>
                        </ul>
                    </div>

                    <!-- Prev・Next ボタン -->
                    <div class="button-set">
                        <?php if ( get_previous_post() ): ?>
                            <?php previous_post_link('%link', '<span><i class="button-prev__icon">PREV</i>%title</span>'); ?>
                        <?php else: ?>
                            <a href="" class="button-prev is-disabled">
                                <span>
                                    <i class="button-prev__icon">PREV</i>
                                    記事がありません
                                </span>
                            </a>
                        <?php endif; ?>
                        <?php if ( get_next_post() ): ?>
                            <?php next_post_link('%link', '<span><i class="button-prev__icon">NEXT</i>%title</span>'); ?>
                        <?php else: ?>
                            <a href="" class="button-next is-disabled">
                                <span>
                                    記事がありません
                                    <i class="button-prev__icon">NEXT</i>
                                </span>
                            </a>
                        <?php endif; ?>
                    </div>
                    <!-- /Prev・Next ボタン -->

                </section>
            </main>
            <!-- aside_nav -->
            <?php get_template_part('_inc/aside_nav'); ?>
        </div>
    </div>
    <!-- /content -->

    <!-- footer -->
    <?php get_template_part('_inc/footer'); ?>
    <?php wp_footer(); ?>
</body>
</html>