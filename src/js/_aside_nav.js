
const AsideNavScroll = (() => {

    window.addEventListener( 'DOMContentLoaded', () => {

        const wrapper = document.querySelector('[data-wrapper]');
        const main = document.querySelector('[data-main]');
        const asideNav = document.querySelector('[data-aside-nav]');

        if (main === null || main === undefined && asideNav === null || asideNav === undefined) { return }

        const breakpointPC = 1105;

        const overflowClass = () => {
            if (breakpointPC <= window.innerWidth && main.clientHeight < asideNav.clientHeight) {
                asideNav.style.height = `${main.clientHeight}px`;
                wrapper.classList.remove('js-flex-stretch');
                asideNav.classList.add('js-scroll');
            } else {
                asideNav.style.height = '';
                wrapper.classList.add('js-flex-stretch');
                asideNav.classList.remove('js-scroll');
            }
        }

        window.addEventListener('load', overflowClass, false);
        window.addEventListener('resize', overflowClass, false);
        window.addEventListener('orientationchange', overflowClass, false);

    }, false);
    
})();

export default AsideNavScroll;