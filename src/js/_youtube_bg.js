const YoutubeBg = (() => {
    window.addEventListener('load', _ => {

        // 上下左右に出てくる黒帯を非表示
        const youtube = document.getElementById('youtube');
        const WIN = window;
        let WIN_H;
        let WIN_W;
        
        function yt_screen_ratio() {
            WIN_H = WIN.innerHeight;
            WIN_W = WIN.innerWidth;
            let screen_switch = 0.5625;
            let screen_ratio = WIN_H / WIN_W;
            let ratio_H = WIN_H / screen_switch;
            let ratio_W = WIN_W * screen_switch;
            
            if(screen_ratio > screen_switch){
                Object.assign(youtube.style,{
                    height: '100%',
                    width: `${ratio_H}px`,
                    marginTop: '0',
                    marginLeft: `-${ratio_H / 2}px`,
                    left: '50%',
                    top: '0'
                });
            } else {
                Object.assign(youtube.style,{
                    width: '100%',
                    height: `${ratio_W}px`,
                    marginTop: `-${ratio_W / 2}px`,
                    marginLeft: '0',
                    top: '50%',
                    left: '0'
                });
            }
        }
        
        yt_screen_ratio();
        window.addEventListener('resize', yt_screen_ratio, false);
        window.addEventListener('orientationchange', yt_screen_ratio, false);

    },false);
})();

const TriangleBg = (() => {
    window.addEventListener('DOMContentLoaded', _ => {

        const bgElement = document.querySelector('[data-bg-triangle]');
        const breakpointPC = 1105;

        if ( bgElement === null || bgElement === undefined ) { return }

        function calc_border_width() {
            let windowW = window.innerWidth;
            let marginL = 80;
            if ( windowW >= breakpointPC ) {
                bgElement.style.borderRightWidth = `${windowW - marginL}px`
            } else {
                bgElement.style.borderRightWidth = `${windowW}px`;
            }
        }

        window.addEventListener('load', calc_border_width, false);
        window.addEventListener('resize', calc_border_width, false);
        window.addEventListener('orientationchange', calc_border_width, false);

    },false);
})();