export const GlobalNav = (() => {
    window.addEventListener( 'DOMContentLoaded', () => {

        const nav = document.querySelector( '[data-global-nav]' );
        let positionY = 0;

        if ( nav === null || nav === undefined ) { return }

        window.addEventListener( 'scroll', () => {

            const navHeight = nav.clientHeight;

            if ( window.scrollY < positionY ) {
                // 上スクロール時の処理
                nav.classList.remove( 'js-hide' );
            } else {
                // 下スクロール時の処理
                nav.classList.add( 'js-hide' );
            }
            // スクロールが停止した位置を保持
            positionY = window.scrollY;

            if ( window.scrollY < navHeight ) {
                nav.classList.remove( 'js-hide' );
            }

        }, false);

    }, false);
})();