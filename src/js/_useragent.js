const UserAgent = (() => {

    window.addEventListener('DOMContentLoaded', () => {

        const body = document.querySelector('body');
        // const ua = navigator.userAgentData;
        // console.log(ua);

        if (navigator.userAgent.indexOf('iPhone') > 0 || navigator.userAgent.indexOf('Android') > 0 && navigator.userAgent.indexOf('Mobile') > 0) {
            // スマートフォン向けの記述
            body.classList.add('sm-device');
        } else if (navigator.userAgent.indexOf('iPad') > 0 || navigator.userAgent.indexOf('Android') > 0) {
            // タブレット向けの記述
            body.classList.add('tb-device');
        } else {
            // PC向けの記述
            body.classList.add('pc-device');
        }

    }, false);

})();

export default UserAgent;