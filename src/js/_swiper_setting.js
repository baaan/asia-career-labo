import Swiper from "swiper/bundle";

export const SwiperHero = (() => {
  window.addEventListener('load', () => {
    const swiper = new Swiper('#swiper-hero', {
        // Default parameters
        slidesPerView: 'auto',
        spaceBetween: 20,
        speed: 500,
        loop: true,
        updateOnWindowResize: true,
        centeredSlides: true,
        pagination: {
            el: '.swiper-pagination',
            bulletElement: 'li',
            type: 'bullets',
            clickable: true,
        },
        autoplay: {
          delay: 5000,
          disableOnInteraction: false,
        },
        // Responsive breakpoints
        breakpoints: {
          // when window width is >= 600px
          600: {
            navigation: {
                nextEl: ".arrow-button--next",
                prevEl: ".arrow-button--prev",
            },
          },
          // when window width is >= 1105px
          1105: {
            navigation: {
                nextEl: ".arrow-button--next",
                prevEl: ".arrow-button--prev",
            },
          },
        }
      });
  },false);
})();

export const SwiperHeroAbout = (() => {
  window.addEventListener('DOMContentLoaded', () => {

    const hero = document.querySelector('[data-slider-about]');

    if ( hero === null ) { return }

    let height = window.innerHeight;

    function calc_hero_height() {
      height = window.innerHeight;
      hero.style.height = `${height}px`;
    }

    calc_hero_height();
    window.addEventListener('resize', calc_hero_height, false);
    window.addEventListener('orientationchange', calc_hero_height, false);

    window.addEventListener('load', () => {
      const swiper = new Swiper('#swiper-hero-about', {
          loop: true,
          speed: 2000,
          effect: 'fade',
          updateOnWindowResize: true,
          fadeEffect: {
              crossFade: true
          },
          autoplay: {
            delay: 4000,
            disableOnInteraction: false,
          },
        });
    },false);

  },false);
})();

export const SwiperAuthor = (() => {
    window.addEventListener('load', () => {
        const swiper = new Swiper('#swiper-author', {
            // Default parameters
            slidesPerView: 'auto',
            slidesPerGroup: 1,
            loop: true,
            freeMode: true,
            updateOnWindowResize: true,
            navigation: {
                nextEl: ".arrow-button--next",
            },
            // Responsive breakpoints
            breakpoints: {
              // when window width is >= 600px
              600: {
                navigation: {
                    nextEl: ".arrow-button--next",
                    prevEl: ".arrow-button--prev",
                },
              },
            }
          })
    },false);
})();

export const SwiperEvent = (() => {
    window.addEventListener('load', () => {
        const swiper = new Swiper('#swiper-event', {
            allowTouchMove: false,
            updateOnWindowResize: true,
            breakpoints: {
              // when window width is >= 600px
              600: {
                slidesPerView: 'auto',
                slidesPerGroup: 1,
                loop: true,
                freeMode: true,
                navigation: {
                    nextEl: ".arrow-button--next",
                    prevEl: ".arrow-button--prev",
                },
              },
            }
          })
    },false);
})();

export const SwiperYoutube = (() => {
    window.addEventListener('load', () => {
        const swiper = new Swiper('#swiper-youtube', {
            // Default parameters
            slidesPerView: 'auto',
            loop: true,
            freeMode: true,
            navigation: {
                nextEl: ".arrow-button--next",
            },
            // Responsive breakpoints
            breakpoints: {
              // when window width is >= 600px
              600: {
                navigation: {
                    nextEl: ".arrow-button--next",
                    prevEl: ".arrow-button--prev",
                },
              },
            }
          })
    },false);
})();