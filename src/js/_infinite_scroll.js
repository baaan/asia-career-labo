const infiniteScroll = (() => {

    window.addEventListener('load', () => {

        const list = document.querySelector('[data-card-list]');
        const button = document.querySelector('[data-infinite]');

        if (button === null) { return }

        const domain = location.protocol + '//' + location.host;
        let dir = '';
        let posts_per_page = 8;
        let offset = 6;
        let offset_event = 8;

        if ( domain.includes('asia-career-labo') ) {
            dir = 'wp/';
        }

            const noData = () => {
                button.classList.remove('js-loading');
                button.classList.add('js-hide');
            }

            const formatMonth = (date) => {
                let format = date.slice(0,10).split('/').join('');
                let result;
                if (Number(format.slice(4,6)) < 10) {
                    result = format.slice(5,6)
                } else {
                    result = format.slice(4,6);
                }
                return result;
            }

            const formatDate = (date) => {
                let format = date.slice(0,10).split('/').join('');
                let result = format.slice(6,8);
                return result;
            }

            const formatDateOfWeek = (date) => {
                let format = date.slice(0,10).split('/').join('');
                let year = Number(format.slice(0,4));
                let month = Number(format.slice(5,6)) - 1;
                let day = Number(format.slice(6,8));
                let dayOfWeekArray = [ '日', '月', '火', '水', '木', '金', '土' ] ;
                var dayOfWeek = new Date( year, month , day );
                let result = dayOfWeekArray[dayOfWeek.getDay()];
                return result;
            }

            const closeLabel = (boolean) => {
                if ( boolean === true ) {
                    return '<span class="label label--attention">終了しました</span>';
                } else {
                    return '';
                }
            }

            button.addEventListener('click', () => {

                if ( location.href.includes('event') ) {
                    // EVENT
                    fetch(`${domain}/wp-json/wp/v2/event?order=desc&offset=${offset_event}&per_page=${posts_per_page}&_embed`)
                    .then(
                        button.classList.add('js-loading')
                    )
                    .then(response => response.json())
                    .then(data => {
                        if (!data.length)
                            return noData();
                        else
                        data.map((post) => {
                            let thumbnail = post['featured_image']['src'];
                            // No thumbnail
                            if (String(thumbnail).indexOf('default.png') !== -1) {
                                thumbnail = `${domain}/${dir}wp-content/themes/asia-career-labo/img/no-image.png`;
                            } else {
                                thumbnail = post['featured_image']['src'];
                            }

                            let card = document.createElement('li');
                            card.classList.add('card-event');
                            card.innerHTML = `
                            <a href="${post['link']}" class="card-event__anchor">
                                <div class="card-event__schedule">
                                    <span class="card-event__month">${formatMonth(post['acf']['event_yymmdd'])}</span>
                                    <span class="card-event__placeholder-date">
                                        <span class="card-event__date">${formatDate(post['acf']['event_yymmdd'])}</span><span class="card-event__dateofweek">${formatDateOfWeek(post['acf']['event_yymmdd'])}</span>
                                    </span>
                                    <span class="card-event__placeholder-time">
                                        <time class="card-event__start-time">${post['acf']['event_start_time']}</time><time class="card-event__finish-time">${post['acf']['event_finish_time']}</time>
                                    </span>
                                </div>
                                <figure class="card-event__image-wrap">
                                    <img src="${thumbnail}" alt="" class="card-event__image">
                                </figure>
                                <dl class="card-event__sumarry">
                                    <dt class="card-event__title">${post['title']['rendered']}</dt>
                                    <dd class="card-event__place">${post['acf']['event_place_text']}</dd>
                                </dl>
                                ${closeLabel(post['acf']['event_close'])}
                            </a>
                            `;
                            list.appendChild(card);
                        })
                    })
                    .then(
                        offset_event = offset_event + 8
                    )
                    .then(
                        setTimeout(() => {
                            button.classList.remove('js-loading')
                        }, 2500)
                    ).catch(error => {
                        console.error('通信に失敗しました', error);
                    });
                } else if ( location.href.includes('author') ) {
                    // Author
                    fetch(`${domain}/wp-json/wp/v2/posts?author=${author_id}&per_page=${posts_per_page}&offset=${offset}&order=desc&_embed`)
                    .then(
                        button.classList.add('js-loading')
                    )
                    .then(response => response.json())
                    .then(data => {
                        if (!data.length)
                            return noData();
                        else
                        data.map((post) => {
                            let thumbnail = post['featured_image']['src'];
                            let link = post['link'];
                            let labelColor = null;
                            // No thumbnail
                            if (link.includes('kyujin') && String(thumbnail).indexOf('default.png') !== -1) {
                                thumbnail = `${domain}/${dir}wp-content/themes/asia-career-labo/img/job_no-image.png`;
                            } else if (String(thumbnail).indexOf('default.png') !== -1) {
                                thumbnail = `${domain}/${dir}wp-content/themes/asia-career-labo/img/no-image.png`;
                            } else {
                                thumbnail = post['featured_image']['src'];
                            }
                            // Choose label
                            if (link.includes('kyujin')) {
                                labelColor = 'label--green';
                            } else if (link.includes('shushoku')) {
                                labelColor = 'label--blue';
                            } else if (link.includes('life')) {
                                labelColor = 'label--pink';
                            } else if (link.includes('migration')) {
                                labelColor = 'label--violet';
                            }

                            let card = document.createElement('li');
                            card.classList.add('card-vertical');
                            card.innerHTML = `
                            <a href="${post['link']}" class="card-vertical__anchor">
                                <figure class="card-vertical__image-wrap">
                                    <img src="${thumbnail}" alt="" class="card-vertical__image">
                                    <figcaption class="label ${labelColor}">${post['category_name']}</figcaption>
                                </figure>
                                <dl class="card-vertical__summary">
                                    <dt class="card-vertical__term">
                                        <img src="${post['_embedded']['author'][0]['avatar_urls']['48']}" alt="" class="card-vertical__avater">
                                        <span class="card-vertical__title">${post['title']['rendered']}</span>
                                    </dt>
                                    <dd class="card-vertical__description">
                                        <span class="card-vertical__author">${post['_embedded']['author'][0]['name']}</span>
                                        <time class="card-vertical__date">${post['date'].slice(0,10).split('-').join('.')}</time>
                                    </dd>
                                </dl>
                            </a>
                            `;
                            list.appendChild(card);
                        })
                    })
                    .then(
                        offset = offset + 8
                    )
                    .then(
                        setTimeout(() => {
                            button.classList.remove('js-loading')
                        }, 2500)
                    ).catch(error => {
                        console.error('通信に失敗しました', error);
                    });
                } else if (location.href.includes('tag')) {
                    // Tag
                    fetch(`${domain}/wp-json/wp/v2/posts?tags=${tag_id}&per_page=${posts_per_page}&offset=${offset}&order=desc&_embed`)
                    .then(
                        button.classList.add('js-loading')
                    )
                    .then(response => response.json())
                    .then(data => {
                        if (!data.length)
                            return noData();
                        else
                        data.map((post) => {
                            let thumbnail = post['featured_image']['src'];
                            let link = post['link'];
                            let labelColor = null;
                            // No thumbnail
                            if (link.includes('kyujin') && String(thumbnail).indexOf('default.png') !== -1) {
                                thumbnail = `${domain}/${dir}wp-content/themes/asia-career-labo/img/job_no-image.png`;
                            } else if (String(thumbnail).indexOf('default.png') !== -1) {
                                thumbnail = `${domain}/${dir}wp-content/themes/asia-career-labo/img/no-image.png`;
                            } else {
                                thumbnail = post['featured_image']['src'];
                            }
                            // Choose label
                            if (link.includes('kyujin')) {
                                labelColor = 'label--green';
                            } else if (link.includes('shushoku')) {
                                labelColor = 'label--blue';
                            } else if (link.includes('life')) {
                                labelColor = 'label--pink';
                            } else if (link.includes('migration')) {
                                labelColor = 'label--violet';
                            }

                            let card = document.createElement('li');
                            card.classList.add('card-vertical');
                            card.innerHTML = `
                            <a href="${post['link']}" class="card-vertical__anchor">
                                <figure class="card-vertical__image-wrap">
                                    <img src="${thumbnail}" alt="" class="card-vertical__image">
                                    <figcaption class="label ${labelColor}">${post['category_name']}</figcaption>
                                </figure>
                                <dl class="card-vertical__summary">
                                    <dt class="card-vertical__term">
                                        <img src="${post['_embedded']['author'][0]['avatar_urls']['48']}" alt="" class="card-vertical__avater">
                                        <span class="card-vertical__title">${post['title']['rendered']}</span>
                                    </dt>
                                    <dd class="card-vertical__description">
                                        <span class="card-vertical__author">${post['_embedded']['author'][0]['name']}</span>
                                        <time class="card-vertical__date">${post['date'].slice(0,10).split('-').join('.')}</time>
                                    </dd>
                                </dl>
                            </a>
                            `;
                            list.appendChild(card);
                        })
                    })
                    .then(
                        offset = offset + 8
                    )
                    .then(
                        setTimeout(() => {
                            button.classList.remove('js-loading')
                        }, 2500)
                    ).catch(error => {
                        console.error('通信に失敗しました', error);
                    });
                } else if (location.href.includes('category') && categories_id === null) {
                    // Tag
                    fetch(`${domain}/wp-json/wp/v2/posts?per_page=${posts_per_page}&offset=${offset}&order=desc&_embed`)
                    .then(
                        button.classList.add('js-loading')
                    )
                    .then(response => response.json())
                    .then(data => {
                        if (!data.length)
                            return noData();
                        else
                        data.map((post) => {
                            let thumbnail = post['featured_image']['src'];
                            let link = post['link'];
                            let labelColor = null;
                            // No thumbnail
                            if (link.includes('kyujin') && String(thumbnail).indexOf('default.png') !== -1) {
                                thumbnail = `${domain}/${dir}wp-content/themes/asia-career-labo/img/job_no-image.png`;
                            } else if (String(thumbnail).indexOf('default.png') !== -1) {
                                thumbnail = `${domain}/${dir}wp-content/themes/asia-career-labo/img/no-image.png`;
                            } else {
                                thumbnail = post['featured_image']['src'];
                            }
                            // Choose label
                            if (link.includes('kyujin')) {
                                labelColor = 'label--green';
                            } else if (link.includes('shushoku')) {
                                labelColor = 'label--blue';
                            } else if (link.includes('life')) {
                                labelColor = 'label--pink';
                            } else if (link.includes('migration')) {
                                labelColor = 'label--violet';
                            }

                            let card = document.createElement('li');
                            card.classList.add('card-vertical');
                            card.innerHTML = `
                            <a href="${post['link']}" class="card-vertical__anchor">
                                <figure class="card-vertical__image-wrap">
                                    <img src="${thumbnail}" alt="" class="card-vertical__image">
                                    <figcaption class="label ${labelColor}">${post['category_name']}</figcaption>
                                </figure>
                                <dl class="card-vertical__summary">
                                    <dt class="card-vertical__term">
                                        <img src="${post['_embedded']['author'][0]['avatar_urls']['48']}" alt="" class="card-vertical__avater">
                                        <span class="card-vertical__title">${post['title']['rendered']}</span>
                                    </dt>
                                    <dd class="card-vertical__description">
                                        <span class="card-vertical__author">${post['_embedded']['author'][0]['name']}</span>
                                        <time class="card-vertical__date">${post['date'].slice(0,10).split('-').join('.')}</time>
                                    </dd>
                                </dl>
                            </a>
                            `;
                            list.appendChild(card);
                        })
                    })
                    .then(
                        offset = offset + 8
                    )
                    .then(
                        setTimeout(() => {
                            button.classList.remove('js-loading')
                        }, 2500)
                    ).catch(error => {
                        console.error('通信に失敗しました', error);
                    });
                } else if (location.href.includes('category')) {
                    // Category
                    fetch(`${domain}/wp-json/wp/v2/posts?categories=${categories_id}&order=desc&offset=${offset}&per_page=${posts_per_page}&_embed`)
                    .then(
                        button.classList.add('js-loading')
                    )
                    .then(response => response.json())
                    .then(data => {
                        if (!data.length)
                            return noData();
                        else
                        //console.log(data)
                        data.map((post) => {
                            let thumbnail = post['featured_image']['src'];
                            let link = post['link'];
                            // No thumbnail
                            if (link.includes('kyujin') && String(thumbnail).indexOf('default.png') !== -1) {
                                thumbnail = `${domain}/${dir}wp-content/themes/asia-career-labo/img/job_no-image.png`;
                            } else if (String(thumbnail).indexOf('default.png') !== -1) {
                                thumbnail = `${domain}/${dir}wp-content/themes/asia-career-labo/img/no-image.png`;
                            } else {
                                thumbnail = post['featured_image']['src'];
                            }
                            let card = document.createElement('li');
                            card.classList.add('card-vertical');
                            card.innerHTML = `
                            <a href="${post['link']}" class="card-vertical__anchor">
                                <figure class="card-vertical__image-wrap">
                                    <img src="${thumbnail}" alt="" class="card-vertical__image">
                                </figure>
                                <dl class="card-vertical__summary">
                                    <dt class="card-vertical__term">
                                        <img src="${post['_embedded']['author'][0]['avatar_urls']['48']}" alt="" class="card-vertical__avater">
                                        <span class="card-vertical__title">${post['title']['rendered']}</span>
                                    </dt>
                                    <dd class="card-vertical__description">
                                        <span class="card-vertical__author">${post['_embedded']['author'][0]['name']}</span>
                                        <time class="card-vertical__date">${post['date'].slice(0,10).split('-').join('.')}</time>
                                    </dd>
                                </dl>
                            </a>
                            `;
                            list.appendChild(card);
                        })
                    })
                    .then(
                        offset = offset + 8
                    )
                    .then(
                        setTimeout(() => {
                            button.classList.remove('js-loading')
                        }, 2500)
                    ).catch(error => {
                        console.error('通信に失敗しました', error);
                    });
                } else {
                    // Other
                    fetch(`${domain}/wp-json/wp/v2/posts?categories=${categories_id}&order=desc&offset=${offset}&per_page=${posts_per_page}&_embed`)
                    .then(
                        button.classList.add('js-loading')
                    )
                    .then(response => response.json())
                    .then(data => {
                        if (!data.length)
                            return noData();
                        else
                        //console.log(data)
                        data.map((post) => {
                            let thumbnail = post['featured_image']['src'];
                            let link = post['link'];
                            // No thumbnail
                            if (link.includes('kyujin') && String(thumbnail).indexOf('default.png') !== -1) {
                                thumbnail = `${domain}/${dir}wp-content/themes/asia-career-labo/img/job_no-image.png`;
                            } else if (String(thumbnail).indexOf('default.png') !== -1) {
                                thumbnail = `${domain}/${dir}wp-content/themes/asia-career-labo/img/no-image.png`;
                            } else {
                                thumbnail = post['featured_image']['src'];
                            }
                            let card = document.createElement('li');
                            card.classList.add('card-vertical');
                            card.innerHTML = `
                            <a href="${post['link']}" class="card-vertical__anchor">
                                <figure class="card-vertical__image-wrap">
                                    <img src="${thumbnail}" alt="" class="card-vertical__image">
                                </figure>
                                <dl class="card-vertical__summary">
                                    <dt class="card-vertical__term">
                                        <img src="${post['_embedded']['author'][0]['avatar_urls']['48']}" alt="" class="card-vertical__avater">
                                        <span class="card-vertical__title">${post['title']['rendered']}</span>
                                    </dt>
                                    <dd class="card-vertical__description">
                                        <span class="card-vertical__author">${post['_embedded']['author'][0]['name']}</span>
                                        <time class="card-vertical__date">${post['date'].slice(0,10).split('-').join('.')}</time>
                                    </dd>
                                </dl>
                            </a>
                            `;
                            list.appendChild(card);
                        })
                    })
                    .then(
                        offset = offset + 8
                    )
                    .then(
                        setTimeout(() => {
                            button.classList.remove('js-loading')
                        }, 2500)
                    ).catch(error => {
                        console.error('通信に失敗しました', error);
                    });
                }

        },false);

    },false);

})();

export default infiniteScroll;