import SmoothScroll from 'smooth-scroll';

const PageScroll = (() => {
    
    let scroll = new SmoothScroll('a[href*="#"]');

    window.addEventListener('DOMContentLoaded', () => {

        let target = document.querySelector('[data-page-scroll]');

        if (target === null || target === undefined) {return}

        let windowH = null;

        const windowHCalc = () => {
            windowH = window.innerHeight;
        }

        window.addEventListener('load', windowHCalc, false);
        window.addEventListener('resize', windowHCalc, false);
        window.addEventListener('orientationchange', windowHCalc, false);

        window.addEventListener('scroll', () => {
            if (window.scrollY > windowH) {
                target.classList.add('js-show');
            } else {
                target.classList.remove('js-show');
            }
        },false);

    },false);

})();

export default PageScroll;