const TabScrollX = (() => {
    window.addEventListener('load', () => {

        let tab = document.querySelector('[data-tab-list]');
        let target = document.querySelector('.is-current');
        let aside_nav = document.querySelector('[data-aside-nav]');

        if (tab === null || target === null) { return }

        const currentX = target.getBoundingClientRect().left;
        const currentW = target.clientWidth;
        const aside_navW = aside_nav.clientWidth;

        if (window.innerWidth >= 1105) {
            tab.scrollTo({
                left: currentX - (window.innerWidth - tab.clientWidth) + aside_navW + currentW,
                behavior: 'smooth',
            });
        } else if (window.innerWidth > 768) {
            tab.scrollTo({
                left: currentX - (window.innerWidth - tab.clientWidth) + currentW,
                behavior: 'smooth',
            });
        } else {
            tab.scrollTo({
                left: currentX,
                behavior: 'smooth',
            });
        }

    },false);
})();

export default TabScrollX;