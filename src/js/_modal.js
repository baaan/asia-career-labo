import MicroModal from 'micromodal';

const Modal = (() => {
    window.addEventListener('load', () => {
        MicroModal.init({
            onShow: modal => document.querySelector('body').classList.add('js-modal-open'),
            onClose: modal => document.querySelector('body').classList.remove('js-modal-open'),
            openClass: 'js-open',
            disableScroll: true,
            awaitOpenAnimation: true,
            awaitCloseAnimation: true,
        });
        const heightCalc = () => {
            const windowW = window.innerWidth;
            const windowH = window.innerHeight;
            const breakpointTB = 600;
            let marginVertical = null;
            const modal = document.querySelectorAll('[data-modal-content]');
            if (windowW < breakpointTB) {
                marginVertical = 32;
            } else {
                marginVertical = 48;
            }
            [...modal].map((elem) => {
                elem.style.maxHeight = `${windowH - marginVertical}px`;
            });
        }
        heightCalc();
        window.addEventListener('resize', heightCalc, false);
        window.addEventListener('orientationchange', heightCalc, false);
    },false);
})();

export default Modal;
