import UserAgent from './js/_useragent';
import GlobalNav from './js/_global_nav';
import AsideNavScroll from './js/_aside_nav';
import PageScroll from './js/_page_scroll';
import {SwiperHero, SwiperAuthor, SwiperEvent, SwiperYoutube} from './js/_swiper_setting';