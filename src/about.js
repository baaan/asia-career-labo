import UserAgent from './js/_useragent';
import GlobalNav from './js/_global_nav';
import PageScroll from './js/_page_scroll';
import {SwiperHeroAbout} from './js/_swiper_setting';
import {YoutubeBg, TriangleBg} from './js/_youtube_bg';