module.exports = {
    mode: 'development',
    devServer: {
      static: {
        directory: `${__dirname}/themes/asia-career-labo/`,
      },
      open: true,
    },
    // メインとなるJavaScriptファイル（エントリーポイント）
    entry: {
        home: './src/home.js',
        about: './src/about.js',
        contact: './src/contact.js',
        page: './src/page.js',
        single: './src/single.js',
        author: './src/author.js',
        event: './src/event.js',
        category: './src/category.js',
        tag: './src/tag.js',
        template: './src/template.js',
        base: './src/base.js',
        form: './src/form.js',
        '404': './src/404.js',
    },
    // ファイルの出力設定
    output: {
      //  出力ファイルのディレクトリ名
      path: `${__dirname}/themes/asia-career-labo/js/`,
      // 出力ファイル名
      filename: "[name].js"
    },
    module: {
        rules: [
          {
            // 拡張子 .js の場合
            test: /\.js$/,
            use: [
              {
                // Babel を利用する
                loader: "babel-loader",
                // Babel のオプションを指定する
                options: {
                  presets: [
                    // プリセットを指定することで、ES2020 を ES5 に変換
                    "@babel/preset-env",
                  ],
                },
              },
            ],
          },
        ],
    },
      // ES5(IE11等)向けの指定
    target: ["web", "es5"],
};