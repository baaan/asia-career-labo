# URL
本番: https://asia-career-labo.com/ <br>
※このリポジトリは本番環境にはdeploy(同期)されてません。FTPのパスワードはサイト運営者に確認してください。

# SETTING
※Docker・NPM前提

1. 当プロジェクトのgitリポジトリをclone
2. `docker-compose up -d`
3. http://localhost:8000/ にブラウザでアクセスしwordpressをインストール
4. データ移行プラグイン`All-in-One WP Migration`などで本番環境からエクスポートしたデータ(ファイル)をローカル環境にインポート
5. `npm i`

# START
※詳細は`package.json`をご確認ください。

* 開発: `npm run dev`
* ビルド: `npm run build`

# Wordpress
※管理画面へのログインはサイト運営者に確認してください。

## ■ページ
### トップページ
https://asia-career-labo.com/

### □カテゴリー一覧
#### 新着記事
https://asia-career-labo.com/category/
#### デフォルト
https://asia-career-labo.com/category/life/
#### ライター
https://asia-career-labo.com/author/baaan/

### タグ一覧
https://asia-career-labo.com/tag/pickup/

### □投稿記事
#### デフォルト
https://asia-career-labo.com/life/investment/hashinary-ssf/
#### イベント
https://asia-career-labo.com/event/brithday-party20210925/

### ライター一覧
https://asia-career-labo.com/author/

### □固定ページ
#### アジラボとは
https://asia-career-labo.com/about/
#### イベント
https://asia-career-labo.com/event/
#### 運用会社
https://asia-career-labo.com/operatingcompany/
#### お問い合わせ
https://asia-career-labo.com/contact/
#### 利用規約
https://asia-career-labo.com/term/
#### プライバシーポリシー
https://asia-career-labo.com/privacy/

### 404
https://asia-career-labo.com/error404/

# MOCK UP
## ■マスターテンプレート
* PCサイドバーあり2カラム
   * https://baaan.gitlab.io/asia-career-labo/_mock/template.html
* PCサイドバーなし1カラム
   * https://baaan.gitlab.io/asia-career-labo/_mock/page.html